package com.epam.servlet.animalClass;

import com.epam.database.exception.DAOException;
import com.epam.database.exception.DBConnectionException;
import com.epam.model.AnimalClass;
import com.epam.servlet.BaseServlet;
import lombok.extern.log4j.Log4j2;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.epam.database.utils.CheckValidParam.idIsNumber;
import static com.epam.database.utils.CheckValidParam.stringIsValid;
import static com.epam.servlet.config.PathJSP.UPDATE_ANIMALCLASS_JSP;

@Log4j2
public class UpdateAnimalClass extends BaseServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String id = req.getParameter("idType");

        if (idIsNumber(id)) {
            try {
                log.debug("The id - {} of animal class is valid.", id);
                AnimalClass animalClass = animalClassDAO.getById(Long.valueOf(id));

                req.setAttribute("animalClass", animalClass);
                log.debug("Set the animal class with id - {} to attribute.", id);

            } catch (DBConnectionException | DAOException e) {
                log.error("Can't to set attribute to update-animalClass.jsp", e);
                throw new ServletException("Can't to set attribute to update-animalClass.jsp", e);
            }
        } else {
            log.warn("The id - {} of animal class is invalid.", id);
            resp.sendRedirect(req.getContextPath() + "/");
            return;
        }
        req.getRequestDispatcher(UPDATE_ANIMALCLASS_JSP.PATH).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        final Long id = Long.valueOf(req.getParameter("idType"));
        final String type = req.getParameter("type");
        AnimalClass animalClass = new AnimalClass(id, type);

        if (stringIsValid(type)) {
            try {
                animalClassDAO.update(animalClass);
                log.debug("Update the {} from the DB - animal_class.", animalClass.toString());
            } catch (DBConnectionException | DAOException e) {
                log.error(String.format("Can't to update the %s from the DB - animal_class.", animalClass.toString()), e);
                throw new ServletException(String.format("Can't to update the %s from the DB - animal_class.", animalClass.toString()));
            }
        } else {
            log.error(String.format("Invalid parameter type - '%s'.", type));
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid parameter type!");
            return;
        }
        resp.sendRedirect(req.getContextPath() + "/");
    }
}
