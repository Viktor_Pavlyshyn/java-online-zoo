package com.epam.servlet.animalClass;

import com.epam.database.exception.DAOException;
import com.epam.database.exception.DBConnectionException;
import com.epam.model.AnimalClass;
import com.epam.model.Zoo;
import com.epam.servlet.BaseServlet;
import lombok.extern.log4j.Log4j2;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import static com.epam.servlet.config.PathJSP.ANIMALCLASS_JSP;

@Log4j2
public class ServletAnimalClass extends BaseServlet {

    private Zoo zoo;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final Long id = Long.valueOf(req.getParameter("id"));
        final String nameZoo = req.getParameter("nameZoo");

        zoo = new Zoo(id, nameZoo);

        resp.sendRedirect(req.getContextPath() + "/animal_type");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            List<AnimalClass> listAnimalClasses = new LinkedList<>();

            List<AnimalClass> allClasses = animalClassDAO.getAll();
            Set<Long> idClasses = animalDAO.getAllClassIdByZoo(zoo);

            for (Long idClass : idClasses) {
                for (AnimalClass animalClass : allClasses) {
                    if (animalClass.getId().equals(idClass)) {
                        listAnimalClasses.add(animalClass);
                    }
                }
            }
            req.setAttribute("zoo", zoo);
            req.setAttribute("animalClasses", listAnimalClasses);
            log.debug("Get all animal classes from the DB - animal_class.");

        } catch (DBConnectionException | DAOException e) {
            log.error("Can't to get all animal classes from the DB - animal_class.", e);
            throw new ServletException("Can't to get all animal classes from the DB - animal_class.", e);
        }

        req.getRequestDispatcher(ANIMALCLASS_JSP.PATH).forward(req, resp);
    }
}
