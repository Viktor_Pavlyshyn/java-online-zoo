package com.epam.servlet.animalClass;

import com.epam.database.exception.DAOException;
import com.epam.database.exception.DBConnectionException;
import com.epam.model.AnimalClass;
import com.epam.servlet.BaseServlet;
import lombok.extern.log4j.Log4j2;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.epam.database.utils.CheckValidParam.stringIsValid;

@Log4j2
public class AddAnimalClass extends BaseServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String type = req.getParameter("type");

        if (stringIsValid(type)) {
            try {
                animalClassDAO.insert(new AnimalClass(type));
                log.debug("Add the {} to DB - animal_class.", type);

            } catch (DBConnectionException | DAOException e) {
                log.error(String.format("Can't to add the %s to DB - animal_class.", type), e);
                throw new ServletException(String.format("Can't to add the %s to DB - animal_class.", type));
            }
        } else {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid parameter type!");
            log.error(String.format("Invalid parameter type - '%s'.", type));
        }
        resp.sendRedirect(req.getContextPath() + "/");
    }
}
