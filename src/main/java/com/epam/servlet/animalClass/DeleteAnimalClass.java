package com.epam.servlet.animalClass;

import com.epam.database.exception.DAOException;
import com.epam.database.exception.DBConnectionException;
import com.epam.model.AnimalClass;
import com.epam.servlet.BaseServlet;
import lombok.extern.log4j.Log4j2;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.epam.database.utils.CheckValidParam.idIsNumber;

@Log4j2
public class DeleteAnimalClass extends BaseServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String id = req.getParameter("typeId");

        if (idIsNumber(id)) {
            log.debug("The id - {} of animal class is valid.", id);
            try {
                AnimalClass animalClass = new AnimalClass(Long.valueOf(id));

                animalDAO.deleteAllByClass(animalClass);
                log.debug("Delete all animals by the animal class with id - {} from the DB - animal.", id);
                animalClassDAO.delete(animalClass);
                log.debug("Delete the animal class with id - {} from the DB - animal_class.", id);

            } catch (DBConnectionException | DAOException e) {
                log.error(String.format("Can't to delete the animal class with id - %s from the DB.", id), e);
                throw new ServletException(String.format("Can't to delete the animal class with id - %s from the DB.", id));
            }
        } else {
            log.warn("The id - {} of animal class is invalid.", id);
        }
        resp.sendRedirect(req.getContextPath() + "/");
    }
}
