package com.epam.servlet.config;

public enum PathJSP {

    ZOO_JSP("/WEB-INF/view/zoo/zoo.jsp"),
    ALL_ZOOS_JSP("/WEB-INF/view/zoo/all-zoos.jsp"),
    UPDATE_ZOO_JSP("/WEB-INF/view/zoo/update-zoo.jsp"),

    ANIMALCLASS_JSP("/WEB-INF/view/animalclass/animalClass.jsp"),
    UPDATE_ANIMALCLASS_JSP("/WEB-INF/view/animalclass/update-animalClass.jsp"),

    ANIMAL_JSP("/WEB-INF/view/animal/animal.jsp"),
    UPDATE_ANIMAL_JSP("/WEB-INF/view/animal/update-animal.jsp"),
    HANDLE_EXCEPTION_JSP("WEB-INF/view/exception/handle-exception.jsp");


    public String PATH;

    PathJSP(String PATH) {
        this.PATH = PATH;
    }
}
