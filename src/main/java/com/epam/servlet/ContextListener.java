package com.epam.servlet;

import com.epam.database.dao.AnimalClassDAO;
import com.epam.database.dao.AnimalDAO;
import com.epam.database.dao.ZooDAO;
import com.epam.database.dao.daoimpl.AnimalClassDAOImpl;
import com.epam.database.dao.daoimpl.AnimalDAOImpl;
import com.epam.database.dao.daoimpl.ZooDAOImpl;
import com.epam.database.utils.DBTable;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.sql.SQLException;

import static com.epam.database.config.SQLTable.*;

public class ContextListener implements ServletContextListener {
    private DBTable dbTable = new DBTable();

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        final ServletContext servletContext = servletContextEvent.getServletContext();

        createDBZoo();

        ZooDAO zooDAO = new ZooDAOImpl();
        AnimalClassDAO animalClassDAO = new AnimalClassDAOImpl();
        AnimalDAO animalDAO = new AnimalDAOImpl();

        servletContext.setAttribute("animalClassDAO", animalClassDAO);
        servletContext.setAttribute("zooDAO", zooDAO);
        servletContext.setAttribute("animalDAO", animalDAO);
    }

    void setDbTable(DBTable dbTable) {
        this.dbTable = dbTable;
    }

    private void createDBZoo() {

        try {
            dbTable.createTable(CREATE_ZOO_TABLE.QUERY);
            dbTable.createTable(CREATE_ANIMAL_CLASS_TABLE.QUERY);
            dbTable.createTable(CREATE_ANIMAL_TABLE.QUERY);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }

}
