package com.epam.servlet.animal;

import com.epam.database.exception.DAOException;
import com.epam.database.exception.DBConnectionException;
import com.epam.database.utils.CheckValidParam;
import com.epam.model.Animal;
import com.epam.model.AnimalClass;
import com.epam.model.Zoo;
import com.epam.servlet.BaseServlet;
import lombok.extern.log4j.Log4j2;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.epam.database.utils.CheckValidParam.stringIsValid;
import static com.epam.servlet.config.PathJSP.UPDATE_ANIMAL_JSP;

@Log4j2
public class UpdateAnimal extends BaseServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String id = req.getParameter("id");
        final String nameZoo = req.getParameter("nameZoo");
        final String type = req.getParameter("type");

        if (CheckValidParam.idIsNumber(id)) {
            try {

                log.debug("The id - {} of animal is valid.", id);

                List<Zoo> listZoos = zooDAO.getAll();
                log.debug("Get all zoos the from DB - zoo.");

                List<AnimalClass> animalClassList = animalClassDAO.getAll();
                log.debug("Get all animal classes the from DB - animalClass.");

                Animal animal = animalDAO.getById(Long.valueOf(id));
                log.debug("Get all animal the from DB - animal.");

                req.setAttribute("zoos", listZoos);
                req.setAttribute("animalClasses", animalClassList);
                req.setAttribute("animal", animal);
                log.debug("Set the attribute to update-animal.jsp.");

            } catch (DBConnectionException | DAOException e) {
                log.error("Can't to get data from the DB.", e);
                throw new ServletException("Can't to get data from the DB.");
            }
        } else {
            log.warn("The id - '{}' of animal is invalid.", id);
            resp.sendRedirect(req.getContextPath() + "/animal");
            return;
        }
        req.setAttribute("nameZoo", nameZoo);
        req.setAttribute("type", type);

        req.getRequestDispatcher(UPDATE_ANIMAL_JSP.PATH).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        final Long id = Long.valueOf(req.getParameter("id"));
        final Long nameZoo = Long.valueOf(req.getParameter("idZoo"));
        final Long type = Long.valueOf(req.getParameter("idType"));
        final String animalName = req.getParameter("animalName");
        final String appearance = req.getParameter("appearance");
        Animal animal = new Animal(id, new Zoo(nameZoo), new AnimalClass(type), animalName, appearance);

        if (stringIsValid(animalName) && stringIsValid(appearance)) {
            try {
                animalDAO.update(animal);
                log.debug("Update the {} from the DB - animal.", animal.toString());

            } catch (DBConnectionException | DAOException e) {
                log.error(String.format("Can't to update the %s from the DB - animal.", animal.toString()), e);
                throw new ServletException(String.format("Can't to update the %s from the DB - animal.", animal.toString()));
            }
        } else {
            log.error("Invalid parameter animalName or appearance.");
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid parameter animalName or appearance!");
            return;
        }
        resp.sendRedirect(req.getContextPath() + "/animal");
    }
}
