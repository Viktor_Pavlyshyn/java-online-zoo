package com.epam.servlet.animal;

import com.epam.database.exception.DAOException;
import com.epam.database.exception.DBConnectionException;
import com.epam.model.Animal;
import com.epam.servlet.BaseServlet;
import lombok.extern.log4j.Log4j2;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.epam.database.utils.CheckValidParam.idIsNumber;

@Log4j2
public class DeleteAnimal extends BaseServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String id = req.getParameter("id");

        if (idIsNumber(id)) {
            log.debug("The id - {} of animal is valid.", id);
            try {
                Animal animal = new Animal();
                animal.setId(Long.valueOf(id));

                animalDAO.delete(animal);
                log.debug("Delete the animal with id - {} from the DB - animal.", id);

            } catch (DBConnectionException | DAOException e) {
                log.error(String.format("Can't to delete the animal with id - %s from the DB - animal.", id), e);
                throw new ServletException(String.format("Can't to delete the animal with id - %s from the DB - animal.", id));
            }
        } else {
            log.warn("The id - '{}' of animal is invalid.", id);
        }
        resp.sendRedirect(req.getContextPath() + "/animal");
    }
}
