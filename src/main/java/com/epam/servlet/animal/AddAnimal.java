package com.epam.servlet.animal;

import com.epam.database.exception.DAOException;
import com.epam.database.exception.DBConnectionException;
import com.epam.model.Animal;
import com.epam.servlet.BaseServlet;
import lombok.extern.log4j.Log4j2;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.epam.database.utils.CheckValidParam.stringIsValid;

@Log4j2
public class AddAnimal extends BaseServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final Long idZoo = Long.valueOf(req.getParameter("idZoo"));
        final Long idType = Long.valueOf(req.getParameter("idType"));
        final String animalName = req.getParameter("animalName");
        final String appearance = req.getParameter("appearance");

        if (stringIsValid(animalName) && stringIsValid(appearance)) {
            try {
                Animal animal = new Animal();
                animal.setNameZoo(zooDAO.getById(idZoo));
                animal.setAnimalClass(animalClassDAO.getById(idType));
                animal.setAnimalName(animalName);
                animal.setAnimalAppearance(appearance);

                animalDAO.insert(animal);
                log.debug("Add the {} to the DB - animal.", animal.toString());

            } catch (DBConnectionException | DAOException e) {
                log.error("Can't to add the animal to the DB - animal.", e);
                throw new ServletException("Can't to add the animal to the DB - animal.");
            }
        } else {
            log.error("Invalid parameter animalName or appearance.");
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid parameter animalName or appearance!");
        }

        resp.sendRedirect(req.getContextPath() + "/");
    }
}
