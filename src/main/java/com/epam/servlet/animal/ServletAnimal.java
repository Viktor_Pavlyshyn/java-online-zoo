package com.epam.servlet.animal;

import com.epam.database.exception.DAOException;
import com.epam.database.exception.DBConnectionException;
import com.epam.model.Animal;
import com.epam.model.AnimalClass;
import com.epam.model.Zoo;
import com.epam.servlet.BaseServlet;
import lombok.extern.log4j.Log4j2;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.epam.servlet.config.PathJSP.ANIMAL_JSP;

@Log4j2
public class ServletAnimal extends BaseServlet {

    private Zoo zoo;
    private AnimalClass animalClass;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        final Long idZoo = Long.valueOf(req.getParameter("idZoo"));
        final String nameZoo = req.getParameter("nameZoo");
        zoo = new Zoo(idZoo, nameZoo);

        final Long idType = Long.valueOf(req.getParameter("idType"));
        final String animalType = req.getParameter("type");
        animalClass = new AnimalClass(idType, animalType);

        resp.sendRedirect(req.getContextPath() + "/animal");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            List<Animal> animals = animalDAO.getAllByZooAndClass(zoo, animalClass);
            log.debug("Get animals by the {} and the {} from the DB - animal.", zoo.toString(), animalClass.toString());

            List<Zoo> zooList = zooDAO.getAll();
            log.debug("Get all zoos from the DB - zoo.");

            List<AnimalClass> animalClassList = animalClassDAO.getAll();
            log.debug("Get all animal class from the DB - animalClass.");

            req.setAttribute("animals", animals);
            req.setAttribute("zoos", zooList);
            req.setAttribute("animalClasses", animalClassList);
            log.debug("Set the attribute to animal.jsp.");

        } catch (DBConnectionException | DAOException e) {
            log.error("Can't to get data from the DB.", e);
            throw new ServletException("Can't to get data from the DB.");
        }
        req.getRequestDispatcher(ANIMAL_JSP.PATH).forward(req, resp);
    }
}
