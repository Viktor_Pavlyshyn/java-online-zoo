package com.epam.servlet.animal;

import com.epam.database.exception.DAOException;
import com.epam.database.exception.DBConnectionException;
import com.epam.model.AnimalClass;
import com.epam.model.Zoo;
import com.epam.servlet.BaseServlet;
import lombok.extern.log4j.Log4j2;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.epam.database.utils.CheckValidParam.idIsNumber;

@Log4j2
public class DeleteAnimalClassFromAnimal extends BaseServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String id = req.getParameter("id");
        final String type = req.getParameter("type");
        final String idZoo = req.getParameter("idZoo");
        final String nameZoo = req.getParameter("nameZoo");

        if (idIsNumber(id) && idIsNumber(idZoo)) {
            log.debug("The id - '{}' of animal class and id - '{}' of zoo is valid.", id, idZoo);

            AnimalClass animalClass = new AnimalClass(Long.valueOf(id), type);
            Zoo zoo = new Zoo(Long.valueOf(idZoo), nameZoo);

            try {
                animalDAO.deleteByClassAndZoo(zoo, animalClass);
                log.debug("Delete the {} from the {} with all animals from the DB - animal.", animalClass.toString(), zoo.toString());

            } catch (DBConnectionException | DAOException e) {
                log.error(String.format("Can't to delete the %s from the %s with all animals from the DB - animal.", animalClass.toString(), zoo.toString()), e);
                throw new ServletException(String.format("Can't to delete the %s from the %s with all animals from the DB - animal.", animalClass.toString(), zoo.toString()));
            }
        } else {
            log.warn("The id - '{}' of animal class or id - '{}' zoo is invalid.", id, idZoo);
        }
        resp.sendRedirect(req.getContextPath() + "/animal_type");
    }
}
