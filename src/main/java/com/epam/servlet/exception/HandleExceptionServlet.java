package com.epam.servlet.exception;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.epam.servlet.config.PathJSP.HANDLE_EXCEPTION_JSP;

public class HandleExceptionServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Throwable throwable = (Throwable) req.getAttribute("javax.servlet.error.exception");
        Integer statusCode = (Integer) req.getAttribute("javax.servlet.error.status_code");
        String servletName = (String) req.getAttribute("javax.servlet.error.servlet_name");

        req.setAttribute("statusCode", statusCode);
        req.setAttribute("servletName", servletName);
        req.setAttribute("exceptionType", throwable.getClass().getName());
        req.setAttribute("exceptionMessage", throwable.getMessage());

        req.getRequestDispatcher(HANDLE_EXCEPTION_JSP.PATH).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
