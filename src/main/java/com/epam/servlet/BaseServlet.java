package com.epam.servlet;

import com.epam.database.dao.AnimalClassDAO;
import com.epam.database.dao.AnimalDAO;
import com.epam.database.dao.ZooDAO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

public abstract class BaseServlet extends HttpServlet {
    protected AnimalDAO animalDAO;
    protected ZooDAO zooDAO;
    protected AnimalClassDAO animalClassDAO;

    @Override
    public void init() throws ServletException {
        this.zooDAO = (ZooDAO) getServletContext().getAttribute("zooDAO");
        this.animalClassDAO = (AnimalClassDAO) getServletContext().getAttribute("animalClassDAO");
        this.animalDAO = (AnimalDAO) getServletContext().getAttribute("animalDAO");
    }
}
