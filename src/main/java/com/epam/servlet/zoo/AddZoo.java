package com.epam.servlet.zoo;

import com.epam.database.exception.DAOException;
import com.epam.database.exception.DBConnectionException;
import com.epam.model.Zoo;
import com.epam.servlet.BaseServlet;
import lombok.extern.log4j.Log4j2;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.epam.database.utils.CheckValidParam.stringIsValid;

@Log4j2
public class AddZoo extends BaseServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String name = req.getParameter("nameZoo");
        Zoo zoo = new Zoo();
        zoo.setNameZoo(name);

        if (stringIsValid(name)) {
            try {
                zooDAO.insert(zoo);
                log.debug("Add the {} to the DB - zoo.", name);
            } catch (DBConnectionException | DAOException e) {
                log.error(String.format("Can't to add the %s to the DB - zoo.", name), e);
                throw new ServletException(String.format("Can't to add the %s to the DB - zoo.", name));
            }
        } else {
            log.error(String.format("Invalid parameter nameZoo - '%s'.", name));
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid parameter name zoo!");
        }
        resp.sendRedirect(req.getContextPath() + "/");
    }
}
