package com.epam.servlet.zoo;

import com.epam.database.exception.DAOException;
import com.epam.database.exception.DBConnectionException;
import com.epam.model.Zoo;
import com.epam.servlet.BaseServlet;
import lombok.extern.log4j.Log4j2;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.epam.database.utils.CheckValidParam.idIsNumber;

@Log4j2
public class DeleteZoo extends BaseServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String id = req.getParameter("idZoo");

        if (idIsNumber(id)) {
            log.debug("The id - {} of zoo is valid.", id);
            try {
                Zoo zoo = new Zoo();
                zoo.setId(Long.valueOf(id));

                animalDAO.deleteAllByZoo(zoo);
                log.debug("Delete all animals with zoo id - {} from the DB - animal.", id);

                zooDAO.delete(zoo);
                log.debug("Delete the zoo with id - {} from the DB - zoo.", id);

            } catch (DBConnectionException | DAOException e) {
                log.error(String.format("Can't to delete the zoo with id - %s the from DB.", id), e);
                throw new ServletException(String.format("Can't to delete the zoo with id - %s the from DB.", id));
            }
        } else {
            log.warn("The id - '{}' of zoo is invalid.", id);
        }
        resp.sendRedirect(req.getContextPath() + "/");
    }
}
