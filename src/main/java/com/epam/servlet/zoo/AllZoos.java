package com.epam.servlet.zoo;

import com.epam.database.exception.DAOException;
import com.epam.database.exception.DBConnectionException;
import com.epam.model.Zoo;
import com.epam.servlet.BaseServlet;
import lombok.extern.log4j.Log4j2;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.epam.servlet.config.PathJSP.ALL_ZOOS_JSP;

@Log4j2
public class AllZoos extends BaseServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            List<Zoo> zoos = zooDAO.getAll();
            log.debug("Get {} from the DB - zoo.", zoos);
            req.setAttribute("zoos", zoos);

        } catch (DBConnectionException | DAOException e) {
            log.error("Can't to get all zoos from the DB - zoo.", e);
            throw new ServletException("Can't to get all zoos from the DB - zoo.", e);
        }
        req.getRequestDispatcher(ALL_ZOOS_JSP.PATH).forward(req, resp);
    }
}
