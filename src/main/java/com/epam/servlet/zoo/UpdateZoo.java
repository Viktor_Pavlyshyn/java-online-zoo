package com.epam.servlet.zoo;

import com.epam.database.exception.DAOException;
import com.epam.database.exception.DBConnectionException;
import com.epam.model.Zoo;
import com.epam.servlet.BaseServlet;
import lombok.extern.log4j.Log4j2;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.epam.database.utils.CheckValidParam.idIsNumber;
import static com.epam.database.utils.CheckValidParam.stringIsValid;
import static com.epam.servlet.config.PathJSP.UPDATE_ZOO_JSP;

@Log4j2
public class UpdateZoo extends BaseServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        final String id = req.getParameter("idZoo");
        if (idIsNumber(id)) {
            try {
                log.debug("The id - {} of zoo is valid.", id);
                Zoo zoo = zooDAO.getById(Long.valueOf(id));

                req.setAttribute("zoo", zoo);
                log.debug("Set zoo with id - {} to update-zoo.jsp.", zoo.toString());

            } catch (DBConnectionException | DAOException e) {
                log.error("Can't to set the attribute to update-zoo.jsp.", e);
                throw new ServletException("Can't to set the attribute to update-zoo.jsp.");
            }
        } else {
            log.warn("The id - {} of zoo is invalid.", id);
            resp.sendRedirect(req.getContextPath() + "/");
            return;
        }
        req.getRequestDispatcher(UPDATE_ZOO_JSP.PATH).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final Long id = Long.valueOf(req.getParameter("idZoo"));
        final String nameZoo = req.getParameter("nameZoo");
        Zoo zoo = new Zoo(id, nameZoo);

        if (stringIsValid(nameZoo)) {
            try {
                zooDAO.update(zoo);
                log.debug("Update {} from the DB - zoo.", zoo.toString());

            } catch (DBConnectionException | DAOException e) {
                log.error(String.format("Can't to update the %s from DB - zoo.", zoo.toString()), e);
                throw new ServletException(String.format("Can't to update the %s from DB - zoo.", zoo.toString()));
            }
        } else {
            log.error(String.format("Invalid parameter type - '%s'.", nameZoo));
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid parameter name zoo!");
            return;
        }
        resp.sendRedirect(req.getContextPath() + "/");
    }
}
