package com.epam.servlet.zoo;

import com.epam.database.exception.DAOException;
import com.epam.database.exception.DBConnectionException;
import com.epam.model.AnimalClass;
import com.epam.model.Zoo;
import com.epam.servlet.BaseServlet;
import lombok.extern.log4j.Log4j2;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.epam.servlet.config.PathJSP.ZOO_JSP;

@Log4j2
public class ServletZoo extends BaseServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            List<Zoo> zoos = zooDAO.getAll();
            log.debug("Set the list {} to attribute.", zoos);

            List<AnimalClass> animalClasses = animalClassDAO.getAll();
            log.debug("Get the list - {} to attribute.", animalClasses);

            req.setAttribute("zoos", zoos);
            req.setAttribute("animalClasses", animalClasses);

        } catch (DBConnectionException | DAOException e) {
            log.error("Can't to get the zoo or the animal class from DB.", e);
            throw new ServletException("Can't to get the zoo or the animal class from DB.");
        }

        req.getRequestDispatcher(ZOO_JSP.PATH).forward(req, resp);
    }

}
