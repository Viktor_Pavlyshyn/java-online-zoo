package com.epam.database.dao;

import com.epam.model.Zoo;

public interface ZooDAO extends DataCRUD<Zoo>, RepositoryData<Zoo> {

}
