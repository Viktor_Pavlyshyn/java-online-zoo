package com.epam.database.dao;

import com.epam.model.AnimalClass;

public interface AnimalClassDAO extends DataCRUD<AnimalClass>, RepositoryData<AnimalClass> {

}
