package com.epam.database.dao;

import com.epam.database.exception.DAOException;
import com.epam.database.exception.DBConnectionException;

import java.util.List;

public interface RepositoryData<T> {

    List<T> getAll() throws DBConnectionException, DAOException;
}
