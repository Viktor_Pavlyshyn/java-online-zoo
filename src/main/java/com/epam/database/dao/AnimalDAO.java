package com.epam.database.dao;

import com.epam.database.exception.DAOException;
import com.epam.database.exception.DBConnectionException;
import com.epam.model.Animal;
import com.epam.model.AnimalClass;
import com.epam.model.Zoo;

import java.util.List;
import java.util.Set;

public interface AnimalDAO extends DataCRUD<Animal> {

    List<Animal> getAllByZooAndClass(Zoo zoo, AnimalClass animalClass) throws DBConnectionException, DAOException;

    Set<Long> getAllClassIdByZoo(Zoo zoo) throws DBConnectionException, DAOException;

    void deleteAllByZoo(Zoo zoo) throws DBConnectionException, DAOException;

    void deleteAllByClass(AnimalClass animalClass) throws DBConnectionException, DAOException;

    void deleteByClassAndZoo(Zoo zoo, AnimalClass animalClass) throws DBConnectionException, DAOException;
}
