package com.epam.database.utils.dpfactory.dataprop;

import com.epam.database.utils.dpfactory.DataProp;
import com.epam.database.utils.dpfactory.PropertiesReaderDB;

import static com.epam.database.config.PropConstants.*;

public class DataPropMySQL extends PropertiesReaderDB implements DataProp {

    @Override
    public String dbHost() {
        return props.getProperty(DB_HOST);
    }

    @Override
    public int dbPort() {
        return Integer.parseInt(props.getProperty(DB_PORT));
    }

    @Override
    public String dbName() {
        return props.getProperty(DB_NAME);
    }

    @Override
    public String dbUser() {
        return props.getProperty(DB_USER);
    }

    @Override
    public String dbPass() {
        return props.getProperty(DB_PASSWORD);
    }

    @Override
    public String jdbcDriver() {
        return props.getProperty(JDBC_DRIVER);
    }

    @Override
    public String sqlURL() {
        return String.format(props.getProperty(DB_URL), dbHost(), dbPort(), dbName());
    }
}
