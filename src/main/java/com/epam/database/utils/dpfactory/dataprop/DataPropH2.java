package com.epam.database.utils.dpfactory.dataprop;

import com.epam.database.utils.dpfactory.DataProp;
import com.epam.database.utils.dpfactory.PropertiesReaderDB;

import static com.epam.database.config.PropConstants.*;

public class DataPropH2 extends PropertiesReaderDB implements DataProp {

    @Override
    public String dbHost() {
        return props.getProperty(H2_HOST);
    }

    @Override
    public int dbPort() {
        return Integer.parseInt(props.getProperty(H2_PORT));
    }

    @Override
    public String dbName() {
        return props.getProperty(H2_NAME);
    }

    @Override
    public String dbUser() {
        return props.getProperty(H2_USER);
    }

    @Override
    public String dbPass() {
        return props.getProperty(H2_PASSWORD);
    }

    @Override
    public String jdbcDriver() {
        return props.getProperty(H2_DRIVER);
    }

    @Override
    public String sqlURL() {
        return String.format(props.getProperty(H2_URL), dbName());
    }
}
