package com.epam.database.utils.dpfactory;

import com.epam.database.utils.dpfactory.dataprop.DataPropH2;
import com.epam.database.utils.dpfactory.dataprop.DataPropMySQL;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class FactoryDadaProp {

    public static DataProp getDataProp() {
        String databaseName = System.getProperty("dbName", "H2");
        DataProp dataProp = null;
        switch (databaseName) {
            case "H2":
                log.debug("Init database - {}.", databaseName);
                dataProp = new DataPropH2();
                break;
            case "MySQL":
                log.debug("Init database - {}.", databaseName);
                dataProp = new DataPropMySQL();
                break;
            default:
                log.error("Database isn't supported - {}.", databaseName);
        }
        return dataProp;
    }
}
