package com.epam.database.utils.dpfactory;

public interface DataProp {

    String dbHost();

    int dbPort();

    String dbName();

    String dbUser();

    String dbPass();

    String jdbcDriver();

    String sqlURL();
}
