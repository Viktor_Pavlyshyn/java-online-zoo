package com.epam.database.utils;

public class CheckValidParam {

    public static boolean idIsNumber(String id) {
        return id != null && (id.length() > 0) && id.matches("[+]?\\d+");
    }

    public static boolean stringIsValid(final String parameter) {
        return parameter != null && !parameter.trim().isEmpty();
    }
}
