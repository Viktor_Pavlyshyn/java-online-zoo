package com.epam.database.utils;

import com.epam.database.connection.DBConnection;
import com.epam.database.exception.DBConnectionException;
import lombok.extern.log4j.Log4j2;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

@Log4j2
public class DBTable {

    public void createTable(String create) throws SQLException {
        try (Connection conn = DBConnection.getInstance().getConnection()) {
            Statement statement = conn.createStatement();
            statement.executeUpdate(create);
            log.info("The table is created.");

        } catch (SQLException | DBConnectionException e) {
            log.error("Unable to create table.", e);
            throw new SQLException("Can't create table!", e);
        }
    }
}
