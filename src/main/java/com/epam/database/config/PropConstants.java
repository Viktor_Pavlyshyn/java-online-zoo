package com.epam.database.config;

public interface PropConstants {
    //H2
    String H2_USER = "h2.user";
    String H2_PASSWORD = "h2.password";
    String H2_NAME = "h2.database";
    String H2_DRIVER = "h2.jdbc.driver";
    String H2_URL = "h2.url";
    String H2_PORT = "h2.port";
    String H2_HOST = "h2.host";

    //MySQL
    String DB_USER = "mysql.user";
    String DB_PASSWORD = "mysql.password";
    String DB_NAME = "mysql.database";
    String JDBC_DRIVER = "jdbc.driver";
    String DB_URL = "mysql.url";
    String DB_PORT = "mysql.port";
    String DB_HOST = "mysql.host";

}
