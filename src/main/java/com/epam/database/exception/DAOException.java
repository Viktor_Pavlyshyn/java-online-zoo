package com.epam.database.exception;

public class DAOException extends Exception {

    public DAOException(String description, Exception e) {
        super(description, e);
    }

    public DAOException() {
    }
}
