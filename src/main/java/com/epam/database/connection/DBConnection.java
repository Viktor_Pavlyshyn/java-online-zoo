package com.epam.database.connection;

import com.epam.database.exception.DBConnectionException;
import com.epam.database.utils.dpfactory.DataProp;
import lombok.extern.log4j.Log4j2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static com.epam.database.utils.dpfactory.FactoryDadaProp.getDataProp;

@Log4j2
public class DBConnection {
    private DataProp dataProp = getDataProp();

    private DBConnection() {
    }

    private static DBConnection instance = null;

    public static DBConnection getInstance() {

        if (instance == null) {
            synchronized (DBConnection.class) {
                if (instance == null) {
                    instance = new DBConnection();
                }
            }
        }
        return instance;
    }

    public Connection getConnection() throws DBConnectionException {
        Connection connection;

        try {
            Class.forName(dataProp.jdbcDriver());
            log.debug("Try connect to {} with user: {} and password: {} ...", dataProp.dbName(), dataProp.dbUser(), dataProp.dbPass());

            connection = DriverManager.getConnection(dataProp.sqlURL(), dataProp.dbUser(), dataProp.dbPass());
            log.debug("Connected to {} database successfully!", dataProp.dbName());

        } catch (ClassNotFoundException | SQLException e) {
            log.error(String.format("Class %s not found!", dataProp.jdbcDriver()), e);
            throw new DBConnectionException("Can't to connection to database.", e);
        }
        return connection;
    }
}
