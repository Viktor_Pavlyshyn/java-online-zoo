<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
  <head>
    <title>Zoos</title>
  </head>
  <body>

    <h3>The status code : <c:out value="${requestScope.statusCode}"/> </h3>

    <h2>Error/Exception Information</h2>

    <div>Servlet Name : <c:out value="${requestScope.servletName}"/> </div></br>

    <div>Exception Type : <c:out value="${requestScope.exceptionType}"/> </div></br>

    <div>The exception message: <c:out value="${requestScope.exceptionMessage}"/> </div></br>

  </body>
</html>