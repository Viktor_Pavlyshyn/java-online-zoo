<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
  <head>
    <title>Zoos</title>
  </head>
  <body>
      <h2>Zoo</h2>

     <form method="get" action="all_zoos">
        <input type="submit" value="Show all zoos"/>
     </form>

     <form method="post" action="add_zoo">

       <label>Add a new name zoo:<br><input type="text" name="nameZoo"></label><br>

       <input type="submit" value="Add" name="submit"><br>
     </form>


     <form method="get" action="update_zoo">

      <label> Choose zoo to update:<br> </label><select name="idZoo">
        <c:forEach var="zoo" items="${requestScope.zoos}">
          <option value="${zoo.id}"> ${zoo.nameZoo} </option>
         </c:forEach>
      </select><br>

      <input type="submit" value="Update"/>
     </form>

     <form method="post" action="delete_zoo">

       <label> Choose zoo to delete: </label><br>
       <select name="idZoo">
         <c:forEach var="zoo" items="${requestScope.zoos}">
           <option value="${zoo.id}"> ${zoo.nameZoo} </option>
         </c:forEach>
       </select><br>

       <input type="submit" name="delete" value="Delete zoo with all animals"/>
     </form>

         <hr/>

      <h2>Animal class</h2>

     <form method="post" action="add_animal_type">

             </label> Add a new class of animals:<br> <label><input type="text" name="type"><br>

             <input type="submit" value="Add" name="Submit"><br>
     </form><br>

     <form method="get" action="update_animal_type">

      <label> Choose an animal class to update:</label><br>
            <select name="idType">
              <c:forEach var="type" items="${requestScope.animalClasses}">
                <option value="${type.id}"> ${type.type} </option>
              </c:forEach>
            </select><br>

       <input type="submit" value="Update"/>
     </form>

     <form method="post" action="delete_animal_class">
           <label> Choose a new class of animals:<br> </label>
         <select name="typeId">
           <c:forEach var="type" items="${requestScope.animalClasses}">
             <option value="${type.id}"> ${type.type} </option>
           </c:forEach>
         </select><br>

            <input type="submit" name="delete" value="Delete the animal class with all animals from all zoos"/>
     </form>
         <hr/>

     <h2>Add Animal</h2>
     <form method="post" action="add_animal">

       <label> Choose a new zoo: </label>
       <select name="idZoo">
         <c:forEach var="zoo" items="${requestScope.zoos}">
           <option value="${zoo.id}"> ${zoo.nameZoo} </option>
         </c:forEach>
       </select><br>

        <label> Choose a new class of animals: </label>
       <select name="idType">
         <c:forEach var="type" items="${requestScope.animalClasses}">
           <option value="${type.id}"> ${type.type} </option>
         </c:forEach>
       </select><br>

       <label>Enter a new animal name : <input type="text" name="animalName"></label><br>
       <label>Enter a new description of the appearance : <input type="text" name="appearance"></label><br>

       <input type="submit" value="Add" name="Submit"><br>

     </form>

          <hr/>

   </body>
</html>