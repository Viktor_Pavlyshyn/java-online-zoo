<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Update Zoo</title>
</head>
<body>

    <div>Zoo ID: <c:out value="${requestScope.zoo.id}"/> </div>
    <div>The name of the zoo: <c:out value="${requestScope.zoo.nameZoo}"/> </div>

    <hr/>

    <form method="post" action="update_zoo">

    <label> Enter a new name for the zoo: <input type="text" name="nameZoo"></label><br>

    <input type="number" hidden name="idZoo" value="${requestScope.zoo.id}"/>

    <input type="submit" value="Ok" name="Ok"><br>
</form>
</body>
</html>