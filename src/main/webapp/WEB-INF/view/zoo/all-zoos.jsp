<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
  <head>
    <title>Zoos</title>
  </head>
  <body>
        <h2>All Zoos</h2>

    <c:forEach var="zoo" items="${requestScope.zoos}">

        <div>Zoo ID : <c:out value="${zoo.id}"/> </div>
        <div>The name of the zoo : <c:out value="${zoo.nameZoo}"/> </div>

        <form method="post" action="animal_type">

           <input type="number" hidden name="id" value="${zoo.id}" />
           <input type="text" hidden name="nameZoo" value="${zoo.nameZoo}" />

           <input type="submit" value="Show animal classes"/>
        </form>

       <hr/>

     </c:forEach>

  </body>
</html>