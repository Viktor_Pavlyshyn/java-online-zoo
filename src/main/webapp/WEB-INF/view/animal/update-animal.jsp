<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Update Animal</title>
</head>
<body>

   <div> ID : <c:out value="${animal.id}"/> </div>
   <div> Zoo : <c:out value="${requestScope.nameZoo}"/> </div>
   <div> Class : <c:out value="${requestScope.type}"/> </div>
   <div> Name : <c:out value="${animal.animalName}"/> </div>
   <div> Appearance : <c:out value="${animal.animalAppearance}"/> </div>

    <hr/>

    <form method="post" action="update_animal">
        <label> Choose a new zoo : </label>
       <select name="idZoo">
          <c:forEach var="zoo" items="${requestScope.zoos}">
             <option value="${zoo.id}"> ${zoo.nameZoo} </option>
          </c:forEach>
        </select><br>

        <label> Choose a new class : </label>
       <select name="idType">
         <c:forEach var="type" items="${requestScope.animalClasses}">
             <option value="${type.id}"> ${type.type} </option>
         </c:forEach>
       </select><br>

       <label>Enter a new animal name : <input type="text" name="animalName"></label><br>

       <label>Enter a new description of the appearance : <input type="text" name="appearance"></label><br>

       <input type="number" hidden name="id" value="${requestScope.animal.id}"/>

       <input type="submit" value="Submit" name="Submit"><br>
    </form>
</body>
</html>