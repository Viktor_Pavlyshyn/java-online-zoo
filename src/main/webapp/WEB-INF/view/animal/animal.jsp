<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
  <head>
    <title>Zoo</title>
  </head>
  <body>
    <h1>Animals!</h1>

    <c:forEach var="animal" items="${requestScope.animals}">

      <div> ID: <c:out value="${animal.id}"/> </div>
      <div> Zoo : <c:out value="${animal.nameZoo.nameZoo}"/> </div>
      <div> Class : <c:out value="${animal.animalClass.type}"/> </div>
      <div> Name : <c:out value="${animal.animalName}"/> </div>
      <div> Appearance : <c:out value="${animal.animalAppearance}"/> </div>

      <form method="post" action="delete_animal">

        <input type="number" hidden name="id" value="${animal.id}" />

        <input type="submit" name="delete" value="Delete"/>
      </form>

      <form method="get" action="update_animal">

        <input type="number" hidden name="id" value="${animal.id}" />
        <input type="text" hidden name="nameZoo" value="${animal.nameZoo.nameZoo}" />
        <input type="text" hidden name="type" value="${animal.animalClass.type}" />

        <input type="submit" value="Update"/>
      </form>

            <hr/>

    </c:forEach>

  </body>
</html>