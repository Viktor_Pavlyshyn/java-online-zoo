<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
  <head>
    <title>Animal class</title>
  </head>
  <body>
    <h1>Class animal</h1>

    <c:forEach var="animalClass" items="${requestScope.animalClasses}">

        <div>Animal class ID : <c:out value="${animalClass.id}"/> </div>
        <div>Animal class : <c:out value="${animalClass.type}"/> </div>

        <form method="post" action="animal">

           <input type="number" hidden name="idZoo" value="${requestScope.zoo.id}" />
           <input type="text" hidden name="nameZoo" value="${requestScope.zoo.nameZoo}" />

           <input type="number" hidden name="idType" value="${animalClass.id}" />
           <input type="text" hidden name="type" value="${animalClass.type}" />

           <input type="submit" value="Show animals"/>
        </form>

        <form method="post" action="delete_animal_class_from_animal">

           <input type="number" hidden name="idZoo" value="${requestScope.zoo.id}" />
           <input type="text" hidden name="nameZoo" value="${requestScope.zoo.nameZoo}" />

           <input type="number" hidden name="id" value="${animalClass.id}" />
           <input type="text" hidden name="type" value="${animalClass.type}" />

           <input type="submit" name="delete" value="Delete the animal class in the current zoo with all animals"/>
        </form>

       <hr/>

     </c:forEach>

  </body>
</html>