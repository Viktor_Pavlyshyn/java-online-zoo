<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Update Class Animal</title>
</head>
<body>

    <div>Animal class ID : <c:out value="${requestScope.animalClass.id}"/> </div>
    <div>Animal class : <c:out value="${requestScope.animalClass.type}"/> </div>

    <hr/>

    <form method="post" action="update_animal_type">

    <label>Enter new animal class : <input type="text" name="type" /></label><br>

    <input type="number" hidden name="idType" value="${requestScope.animalClass.id}"/>

    <input type="submit" value="Ok" name="Ok"><br>
</form>
</body>
</html>