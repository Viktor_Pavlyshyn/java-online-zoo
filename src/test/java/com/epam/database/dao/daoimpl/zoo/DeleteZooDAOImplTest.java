package com.epam.database.dao.daoimpl.zoo;

import com.epam.database.dao.daoimpl.DAOMockTest;
import com.epam.database.dao.daoimpl.ZooDAOImpl;
import com.epam.database.exception.DAOException;
import com.epam.model.Zoo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.SQLException;

import static com.epam.database.config.SQLProp.DELETE_ZOO;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DeleteZooDAOImplTest extends DAOMockTest {

    @Before
    public void setUp() throws Exception {
        zooDAO = new ZooDAOImpl(mockDBConnection);
        zoo = new Zoo((long) 1, "test-zoo");

        when(mockDBConnection.getConnection()).thenReturn(mockConnection);
        when(mockConnection.prepareStatement(DELETE_ZOO.QUERY)).thenReturn(mockPrStatement);

        doNothing().when(mockPrStatement).setLong(eq(1), anyInt());
    }

    @Test
    public void deleteNoException() throws Exception {
        zooDAO.delete(zoo);

        verify(mockDBConnection, times(1)).getConnection();
        verify(mockConnection, times(1)).prepareStatement(DELETE_ZOO.QUERY);

        verify(mockPrStatement, times(1)).setLong(eq(1), anyInt());

        verify(mockPrStatement, times(1)).executeUpdate();
    }

    @Test(expected = DAOException.class)
    public void deletePrStatementException() throws Exception {
        doThrow(new SQLException()).when(mockPrStatement).setLong(eq(1), anyInt());
        try {
            zooDAO.delete(zoo);
        } catch (DAOException e) {

            verify(mockDBConnection, times(1)).getConnection();
            verify(mockConnection, times(1)).prepareStatement(DELETE_ZOO.QUERY);

            verify(mockPrStatement, times(1)).setLong(eq(1), anyInt());

            verify(mockPrStatement, times(0)).executeUpdate();
            throw e;
        }
    }
}