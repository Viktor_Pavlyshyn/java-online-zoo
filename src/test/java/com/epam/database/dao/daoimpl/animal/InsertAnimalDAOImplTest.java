package com.epam.database.dao.daoimpl.animal;

import com.epam.database.dao.daoimpl.AnimalDAOImpl;
import com.epam.database.dao.daoimpl.DAOMockTest;
import com.epam.database.exception.DAOException;
import com.epam.model.Animal;
import com.epam.model.AnimalClass;
import com.epam.model.Zoo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.SQLException;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class InsertAnimalDAOImplTest extends DAOMockTest {

    @Before
    public void setUp() throws Exception {
        animalDAO = new AnimalDAOImpl(mockDBConnection);
        animal = new Animal((long) 1, new Zoo((long) 1, "test-zoo"),
                new AnimalClass((long) 1, "test-class"), "test-name", "test-app");

        when(mockDBConnection.getConnection()).thenReturn(mockConnection);
        when(mockConnection.prepareStatement(anyString())).thenReturn(mockPrStatement);


        doNothing().when(mockPrStatement).setLong(anyInt(), anyLong());
        doNothing().when(mockPrStatement).setString(anyInt(), anyString());

        when(mockPrStatement.executeUpdate()).thenReturn(1);
    }

    @Test
    public void insertNoExceptions() throws Exception {
        animalDAO.insert(animal);

        verify(mockDBConnection, times(1)).getConnection();
        verify(mockConnection, times(1)).prepareStatement(anyString());
        verify(mockPrStatement, times(2)).setLong(anyInt(), anyLong());
        verify(mockPrStatement, times(2)).setString(anyInt(), anyString());
        verify(mockPrStatement, times(1)).executeUpdate();
    }

    @Test(expected = DAOException.class)
    public void insertPrStatementExceptions() throws Exception {
        when(mockConnection.prepareStatement(anyString())).thenThrow(new SQLException());

        try {
            animalDAO.insert(animal);
        } catch (DAOException e) {
            verify(mockDBConnection, times(1)).getConnection();
            verify(mockConnection, times(1)).prepareStatement(anyString());
            verify(mockPrStatement, times(0)).setLong(anyInt(), anyLong());
            verify(mockPrStatement, times(0)).setString(anyInt(), anyString());
            verify(mockPrStatement, times(0)).executeUpdate();
            throw e;
        }
    }

    @Test(expected = DAOException.class)
    public void insertExecuteUpdateExceptions() throws Exception {
        when(mockPrStatement.executeUpdate()).thenThrow(new SQLException());

        try {
            animalDAO.insert(animal);
        } catch (DAOException e) {
            verify(mockDBConnection, times(1)).getConnection();
            verify(mockConnection, times(1)).prepareStatement(anyString());
            verify(mockPrStatement, times(2)).setLong(anyInt(), anyLong());
            verify(mockPrStatement, times(2)).setString(anyInt(), anyString());
            verify(mockPrStatement, times(1)).executeUpdate();
            throw e;
        }
    }

}