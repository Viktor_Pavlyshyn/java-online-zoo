package com.epam.database.dao.daoimpl.zoo;

import com.epam.database.dao.daoimpl.DAOMockTest;
import com.epam.database.dao.daoimpl.ZooDAOImpl;
import com.epam.database.exception.DAOException;
import com.epam.model.Zoo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.SQLException;
import java.util.LinkedList;

import static com.epam.database.config.SQLProp.SELECT_ALL_ZOO;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class GetAllZooDAOImplTest extends DAOMockTest {

    @Before
    public void setUp() throws Exception {
        zooDAO = new ZooDAOImpl(mockDBConnection);
        listZoos = new LinkedList<>();
        listZoos.add(new Zoo((long) 1, "test-zoo"));

        when(mockDBConnection.getConnection()).thenReturn(mockConnection);
        when(mockConnection.createStatement()).thenReturn(mockStatement);
        when(mockStatement.executeQuery(SELECT_ALL_ZOO.QUERY)).thenReturn(mockResultSet);

        when(mockResultSet.next()).thenReturn(true).thenReturn(false);
        when(mockResultSet.getLong(1)).thenReturn((long) 1);
        when(mockResultSet.getString(2)).thenReturn("test-zoo");

    }

    @Test
    public void getAllNoException() throws Exception {
        zooDAO.getAll();

        verify(mockDBConnection, times(1)).getConnection();
        verify(mockConnection, times(1)).createStatement();
        verify(mockStatement, times(1)).executeQuery(SELECT_ALL_ZOO.QUERY);

        verify(mockResultSet, times(2)).next();
        verify(mockResultSet, times(1)).getLong(1);
        verify(mockResultSet, times(1)).getString(2);
    }

    @Test(expected = DAOException.class)
    public void getAllResultSetException() throws Exception {
        when(mockResultSet.getLong(1)).thenThrow(new SQLException());
        try {
            zooDAO.getAll();
        } catch (DAOException e) {
            verify(mockDBConnection, times(1)).getConnection();
            verify(mockConnection, times(1)).createStatement();
            verify(mockStatement, times(1)).executeQuery(SELECT_ALL_ZOO.QUERY);

            verify(mockResultSet, times(1)).next();
            verify(mockResultSet, times(1)).getLong(1);
            verify(mockResultSet, times(0)).getString(2);
            throw e;
        }
    }

}