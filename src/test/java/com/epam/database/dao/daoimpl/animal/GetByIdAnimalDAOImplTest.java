package com.epam.database.dao.daoimpl.animal;

import com.epam.database.dao.daoimpl.AnimalDAOImpl;
import com.epam.database.dao.daoimpl.DAOMockTest;
import com.epam.database.exception.DAOException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.SQLException;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class GetByIdAnimalDAOImplTest extends DAOMockTest {

    @Before
    public void setUp() throws Exception {
        animalDAO = new AnimalDAOImpl(mockDBConnection);

        when(mockDBConnection.getConnection()).thenReturn(mockConnection);
        when(mockConnection.prepareStatement(anyString())).thenReturn(mockPrStatement);
        doNothing().when(mockPrStatement).setLong(eq(1), eq(1));

        when(mockPrStatement.executeQuery()).thenReturn(mockResultSet);
        when(mockResultSet.next()).thenReturn(true);

        when(mockResultSet.getLong(anyInt())).thenReturn(Long.valueOf(1));
        when(mockResultSet.getString(anyInt())).thenReturn("test-animal");
    }

    @Test
    public void getByIdNoException() throws Exception {
        animalDAO.getById(1L);

        verify(mockDBConnection, times(1)).getConnection();

        verify(mockConnection, times(1)).prepareStatement(anyString());
        verify(mockPrStatement, times(1)).setLong(anyInt(), anyLong());

        verify(mockPrStatement, times(1)).executeQuery();
        verify(mockResultSet, times(1)).next();

        verify(mockResultSet, times(3)).getLong(anyInt());
        verify(mockResultSet, times(2)).getString(anyInt());

        verify(mockPrStatement, times(1)).close();
    }

    @Test(expected = DAOException.class)
    public void getByIdPrStatementException() throws Exception {
        when(mockConnection.prepareStatement(anyString())).thenThrow(new SQLException());

        try {
            animalDAO.getById(1L);
        } catch (DAOException e) {
            verify(mockDBConnection, times(1)).getConnection();

            verify(mockConnection, times(1)).prepareStatement(anyString());
            verify(mockPrStatement, times(0)).setLong(anyInt(), anyLong());

            verify(mockPrStatement, times(0)).executeQuery();
            verify(mockResultSet, times(0)).next();

            verify(mockResultSet, times(0)).getLong(anyInt());
            verify(mockResultSet, times(0)).getString(anyInt());

            verify(mockPrStatement, times(0)).close();
            throw e;
        }
    }

    @Test(expected = DAOException.class)
    public void getByIdResultSerException() throws Exception {
        when(mockResultSet.getLong(2)).thenThrow(new SQLException());

        try {
            animalDAO.getById(1L);
        } catch (DAOException e) {
            verify(mockDBConnection, times(1)).getConnection();

            verify(mockConnection, times(1)).prepareStatement(anyString());
            verify(mockPrStatement, times(1)).setLong(anyInt(), anyLong());

            verify(mockPrStatement, times(1)).executeQuery();
            verify(mockResultSet, times(1)).next();

            verify(mockResultSet, times(2)).getLong(anyInt());
            verify(mockResultSet, times(0)).getString(anyInt());

            verify(mockPrStatement, times(1)).close();
            throw e;
        }
    }

}