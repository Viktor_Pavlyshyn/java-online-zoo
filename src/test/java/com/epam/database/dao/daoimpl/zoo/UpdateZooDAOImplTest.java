package com.epam.database.dao.daoimpl.zoo;

import com.epam.database.dao.daoimpl.DAOMockTest;
import com.epam.database.dao.daoimpl.ZooDAOImpl;
import com.epam.database.exception.DAOException;
import com.epam.model.Zoo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.SQLException;

import static com.epam.database.config.SQLProp.UPDATE_ZOO;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UpdateZooDAOImplTest extends DAOMockTest {

    @Before
    public void setUp() throws Exception {
        zooDAO = new ZooDAOImpl(mockDBConnection);
        zoo = new Zoo((long) 1, "test-zoo");

        when(mockDBConnection.getConnection()).thenReturn(mockConnection);
        when(mockConnection.prepareStatement(UPDATE_ZOO.QUERY)).thenReturn(mockPrStatement);

        doNothing().when(mockPrStatement).setString(eq(1), anyString());
        doNothing().when(mockPrStatement).setLong(eq(2), anyInt());

    }

    @Test
    public void updateNoException() throws Exception {
        zooDAO.update(zoo);

        verify(mockDBConnection, times(1)).getConnection();
        verify(mockConnection, times(1)).prepareStatement(UPDATE_ZOO.QUERY);

        verify(mockPrStatement, times(1)).setString(eq(1), anyString());
        verify(mockPrStatement, times(1)).setLong(eq(2), anyInt());

        verify(mockPrStatement, times(1)).executeUpdate();
    }

    @Test(expected = DAOException.class)
    public void updatePrStatementException() throws Exception {
        doThrow(new SQLException()).when(mockPrStatement).setString(eq(1), anyString());
        try {
            zooDAO.update(zoo);
        } catch (DAOException e) {
            verify(mockDBConnection, times(1)).getConnection();
            verify(mockConnection, times(1)).prepareStatement(UPDATE_ZOO.QUERY);

            verify(mockPrStatement, times(1)).setString(eq(1), anyString());
            verify(mockPrStatement, times(0)).setLong(eq(2), anyInt());

            verify(mockPrStatement, times(0)).executeUpdate();
            throw e;
        }
    }
}