package com.epam.database.dao.daoimpl.animal;

import com.epam.database.dao.daoimpl.AnimalDAOImpl;
import com.epam.database.dao.daoimpl.DAOMockTest;
import com.epam.database.exception.DAOException;
import com.epam.model.AnimalClass;
import com.epam.model.Zoo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.SQLException;

import static com.epam.database.config.SQLProp.DELETE_ANIMAL_BY_ANIMAL_ClASS_AND_ZOO;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DeleteByClassAndZooAnimalDAOImplTest extends DAOMockTest {

    @Before
    public void setUp() throws Exception {
        animalDAO = new AnimalDAOImpl(mockDBConnection);
        animalClass = new AnimalClass((long) 1, "test-class");
        zoo = new Zoo((long) 1, "test-zoo");

        when(mockDBConnection.getConnection()).thenReturn(mockConnection);
        when(mockConnection.prepareStatement(DELETE_ANIMAL_BY_ANIMAL_ClASS_AND_ZOO.QUERY)).thenReturn(mockPrStatement);

        doNothing().when(mockPrStatement).setLong(anyInt(), anyInt());

    }

    @Test
    public void deleteAllByZooNoException() throws Exception {
        animalDAO.deleteByClassAndZoo(zoo, animalClass);

        verify(mockDBConnection, times(1)).getConnection();
        verify(mockConnection, times(1)).prepareStatement(DELETE_ANIMAL_BY_ANIMAL_ClASS_AND_ZOO.QUERY);

        verify(mockPrStatement, times(2)).setLong(anyInt(), anyInt());

        verify(mockPrStatement, times(1)).executeUpdate();
    }

    @Test(expected = DAOException.class)
    public void deleteAllByZooPrStatementException() throws Exception {
        doThrow(new SQLException()).when(mockPrStatement).setLong(eq(1), anyInt());

        try {
            animalDAO.deleteByClassAndZoo(zoo, animalClass);
        } catch (DAOException e) {

            verify(mockDBConnection, times(1)).getConnection();
            verify(mockConnection, times(1)).prepareStatement(DELETE_ANIMAL_BY_ANIMAL_ClASS_AND_ZOO.QUERY);

            verify(mockPrStatement, times(1)).setLong(eq(1), anyInt());

            verify(mockPrStatement, times(0)).executeUpdate();
            throw e;
        }
    }

}