package com.epam.database.dao.daoimpl.zoo;

import com.epam.database.dao.daoimpl.DAOMockTest;
import com.epam.database.dao.daoimpl.ZooDAOImpl;
import com.epam.database.exception.DAOException;
import com.epam.model.Zoo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.SQLException;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.eq;

@RunWith(MockitoJUnitRunner.class)
public class GetByIdZooDAOImplTest extends DAOMockTest {

    @Before
    public void setUp() throws Exception {
        zooDAO = new ZooDAOImpl(mockDBConnection);
        zoo = new Zoo((long) 1);

        when(mockDBConnection.getConnection()).thenReturn(mockConnection);
        when(mockConnection.prepareStatement(anyString())).thenReturn(mockPrStatement);
        doNothing().when(mockPrStatement).setLong(eq(1), eq(1));

        when(mockResultSet.next()).thenReturn(true);

        when(mockPrStatement.executeQuery()).thenReturn(mockResultSet);
        when(mockResultSet.getLong(1)).thenReturn(Long.valueOf(1));
        when(mockResultSet.getString(2)).thenReturn("test-nameZoo");
    }

    @Test
    public void getByIdNoException() throws Exception {
        zooDAO.getById(1L);

        verify(mockDBConnection, times(1)).getConnection();

        verify(mockConnection, times(1)).prepareStatement(anyString());
        verify(mockPrStatement, times(1)).setLong(anyInt(), anyLong());

        verify(mockPrStatement, times(1)).executeQuery();
        verify(mockResultSet, times(1)).next();

        verify(mockResultSet, times(1)).getLong(1);
        verify(mockResultSet, times(1)).getString(2);

        verify(mockPrStatement, times(1)).close();
    }

    @Test(expected = DAOException.class)
    public void getByIdPrStatementException() throws Exception {
        when(mockConnection.prepareStatement(anyString())).thenThrow(new SQLException());

        try {
            zooDAO.getById(1L);
        } catch (DAOException e) {
            verify(mockDBConnection, times(1)).getConnection();

            verify(mockConnection, times(1)).prepareStatement(anyString());
            verify(mockPrStatement, times(0)).setLong(anyInt(), anyLong());

            verify(mockPrStatement, times(0)).executeQuery();
            verify(mockResultSet, times(0)).next();

            verify(mockResultSet, times(0)).getLong(1);
            verify(mockResultSet, times(0)).getString(2);

            verify(mockPrStatement, times(0)).close();
            throw e;
        }
    }

    @Test(expected = DAOException.class)
    public void getByIdResultSerException() throws Exception {
        when(mockResultSet.getLong(1)).thenThrow(new SQLException());

        try {
            zooDAO.getById(1L);
        } catch (DAOException e) {
            verify(mockDBConnection, times(1)).getConnection();

            verify(mockConnection, times(1)).prepareStatement(anyString());
            verify(mockPrStatement, times(1)).setLong(anyInt(), anyLong());

            verify(mockPrStatement, times(1)).executeQuery();
            verify(mockResultSet, times(1)).next();

            verify(mockResultSet, times(1)).getLong(1);
            verify(mockResultSet, times(0)).getString(2);

            verify(mockPrStatement, times(1)).close();
            throw e;
        }
    }
}