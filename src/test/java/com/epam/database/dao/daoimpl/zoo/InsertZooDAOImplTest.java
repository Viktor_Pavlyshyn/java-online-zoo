package com.epam.database.dao.daoimpl.zoo;

import com.epam.database.dao.daoimpl.DAOMockTest;
import com.epam.database.dao.daoimpl.ZooDAOImpl;
import com.epam.database.exception.DAOException;
import com.epam.model.Zoo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.SQLException;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class InsertZooDAOImplTest extends DAOMockTest {

    @Before
    public void setUp() throws Exception {
        zooDAO = new ZooDAOImpl(mockDBConnection);

        when(mockDBConnection.getConnection()).thenReturn(mockConnection);
        when(mockConnection.prepareStatement(anyString())).thenReturn(mockPrStatement);
        doNothing().when(mockPrStatement).setString(anyInt(), anyString());
        when(mockPrStatement.executeUpdate()).thenReturn(1);
    }

    @Test
    public void insertNoExceptions() throws Exception {
        zooDAO.insert(new Zoo());

        verify(mockDBConnection, times(1)).getConnection();
        verify(mockConnection, times(1)).prepareStatement(anyString());
        verify(mockPrStatement, times(1)).setString(anyInt(), anyString());
        verify(mockPrStatement, times(1)).executeUpdate();
    }

    @Test(expected = DAOException.class)
    public void insertPrStatementExceptions() throws Exception {
        when(mockConnection.prepareStatement(anyString())).thenThrow(new SQLException());

        try {
            zooDAO.insert(new Zoo());
        } catch (DAOException e) {
            verify(mockDBConnection, times(1)).getConnection();
            verify(mockConnection, times(1)).prepareStatement(anyString());
            verify(mockPrStatement, times(0)).setString(anyInt(), anyString());
            verify(mockPrStatement, times(0)).executeUpdate();
            throw e;
        }
    }

    @Test(expected = DAOException.class)
    public void insertExecuteUpdateExceptions() throws Exception {
        when(mockPrStatement.executeUpdate()).thenThrow(new SQLException());

        try {
            zooDAO.insert(new Zoo());
        } catch (DAOException e) {
            verify(mockDBConnection, times(1)).getConnection();
            verify(mockConnection, times(1)).prepareStatement(anyString());
            verify(mockPrStatement, times(1)).setString(anyInt(), anyString());
            verify(mockPrStatement, times(1)).executeUpdate();
            throw e;
        }
    }
}