package com.epam.database.dao.daoimpl.animalclass;

import com.epam.database.dao.daoimpl.AnimalClassDAOImpl;
import com.epam.database.dao.daoimpl.DAOMockTest;
import com.epam.database.exception.DAOException;
import com.epam.model.AnimalClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.SQLException;

import static com.epam.database.config.SQLProp.DELETE_ANIMAL_CLASS;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DeleteAnimalClassDAOImplTest extends DAOMockTest {

    @Before
    public void setUp() throws Exception {
        animalClassDAO = new AnimalClassDAOImpl(mockDBConnection);
        animalClass = new AnimalClass((long) 1, "test-animalClass");

        when(mockDBConnection.getConnection()).thenReturn(mockConnection);
        when(mockConnection.prepareStatement(DELETE_ANIMAL_CLASS.QUERY)).thenReturn(mockPrStatement);

        doNothing().when(mockPrStatement).setLong(eq(1), anyInt());
    }

    @Test
    public void deleteNoException() throws Exception {
        animalClassDAO.delete(animalClass);

        verify(mockDBConnection, times(1)).getConnection();
        verify(mockConnection, times(1)).prepareStatement(DELETE_ANIMAL_CLASS.QUERY);

        verify(mockPrStatement, times(1)).setLong(eq(1), anyInt());

        verify(mockPrStatement, times(1)).executeUpdate();
    }

    @Test(expected = DAOException.class)
    public void deletePrStatementException() throws Exception {
        doThrow(new SQLException()).when(mockPrStatement).setLong(eq(1), anyInt());

        try {
            animalClassDAO.delete(animalClass);
        } catch (DAOException e) {

            verify(mockDBConnection, times(1)).getConnection();
            verify(mockConnection, times(1)).prepareStatement(DELETE_ANIMAL_CLASS.QUERY);

            verify(mockPrStatement, times(1)).setLong(eq(1), anyInt());

            verify(mockPrStatement, times(0)).executeUpdate();
            throw e;
        }
    }
}