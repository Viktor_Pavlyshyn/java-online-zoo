package com.epam.database.dao.daoimpl;

import com.epam.database.connection.DBConnection;
import com.epam.model.Animal;
import com.epam.model.AnimalClass;
import com.epam.model.Zoo;
import org.mockito.Mock;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

public abstract class DAOMockTest {
    @Mock
    public DBConnection mockDBConnection;
    @Mock
    public Connection mockConnection;
    @Mock
    public PreparedStatement mockPrStatement;
    @Mock
    public Statement mockStatement;
    @Mock
    public ResultSet mockResultSet;

    public ZooDAOImpl zooDAO;
    public AnimalClassDAOImpl animalClassDAO;
    public AnimalDAOImpl animalDAO;
    public Zoo zoo;
    public AnimalClass animalClass;
    public Animal animal;
    public List<Zoo> listZoos;
    public List<AnimalClass> listAnimalClasses;
}
