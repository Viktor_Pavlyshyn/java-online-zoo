package com.epam.database.dao.daoimpl.animalclass;

import com.epam.database.dao.daoimpl.AnimalClassDAOImpl;
import com.epam.database.dao.daoimpl.DAOMockTest;
import com.epam.database.exception.DAOException;
import com.epam.model.AnimalClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.SQLException;

import static com.epam.database.config.SQLProp.UPDATE_ANIMAL_CLASS;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UpdateAnimalClassDAOImplTest extends DAOMockTest {

    @Before
    public void setUp() throws Exception {
        animalClassDAO = new AnimalClassDAOImpl(mockDBConnection);
        animalClass = new AnimalClass((long) 1, "test-animal");

        when(mockDBConnection.getConnection()).thenReturn(mockConnection);
        when(mockConnection.prepareStatement(UPDATE_ANIMAL_CLASS.QUERY)).thenReturn(mockPrStatement);

        doNothing().when(mockPrStatement).setString(eq(1), anyString());
        doNothing().when(mockPrStatement).setLong(eq(2), anyInt());
    }

    @Test
    public void updateNoException() throws Exception {
        animalClassDAO.update(animalClass);

        verify(mockDBConnection, times(1)).getConnection();
        verify(mockConnection, times(1)).prepareStatement(UPDATE_ANIMAL_CLASS.QUERY);

        verify(mockPrStatement, times(1)).setString(eq(1), anyString());
        verify(mockPrStatement, times(1)).setLong(eq(2), anyInt());

        verify(mockPrStatement, times(1)).executeUpdate();
    }

    @Test(expected = DAOException.class)
    public void updatePrStatementException() throws Exception {
        doThrow(new SQLException()).when(mockPrStatement).setString(eq(1), anyString());
        try {
            animalClassDAO.update(animalClass);
        } catch (DAOException e) {
            verify(mockDBConnection, times(1)).getConnection();
            verify(mockConnection, times(1)).prepareStatement(UPDATE_ANIMAL_CLASS.QUERY);

            verify(mockPrStatement, times(1)).setString(eq(1), anyString());
            verify(mockPrStatement, times(0)).setLong(eq(2), anyInt());

            verify(mockPrStatement, times(0)).executeUpdate();
            throw e;
        }
    }
}