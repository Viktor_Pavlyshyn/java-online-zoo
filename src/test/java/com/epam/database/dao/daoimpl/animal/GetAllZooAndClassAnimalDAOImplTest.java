package com.epam.database.dao.daoimpl.animal;

import com.epam.database.dao.daoimpl.AnimalDAOImpl;
import com.epam.database.dao.daoimpl.DAOMockTest;
import com.epam.database.exception.DAOException;
import com.epam.model.AnimalClass;
import com.epam.model.Zoo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.SQLException;

import static com.epam.database.config.SQLProp.SELECT_ALL_ANIMAL_BY_ZOO_AND_CLASS;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class GetAllZooAndClassAnimalDAOImplTest extends DAOMockTest {

    @Before
    public void setUp() throws Exception {
        animalDAO = new AnimalDAOImpl(mockDBConnection);
        zoo = new Zoo((long) 1, "test-zoo");
        animalClass = new AnimalClass((long) 1, "test-class");

        when(mockDBConnection.getConnection()).thenReturn(mockConnection);
        when(mockConnection.prepareStatement(SELECT_ALL_ANIMAL_BY_ZOO_AND_CLASS.QUERY)).thenReturn(mockPrStatement);
        doNothing().when(mockPrStatement).setLong(anyInt(), anyInt());
        when(mockPrStatement.executeQuery()).thenReturn(mockResultSet);

        when(mockResultSet.next()).thenReturn(true).thenReturn(false);

        when(mockResultSet.getLong(1)).thenReturn((long) 1);
        when(mockResultSet.getString(2)).thenReturn("test-nameZoo");
        when(mockResultSet.getString(3)).thenReturn("test-animalClass");
        when(mockResultSet.getString(4)).thenReturn("test-animalName");
        when(mockResultSet.getString(5)).thenReturn("test-appearance");
    }

    @Test
    public void getAllByZooAndClassNoException() throws Exception {
        animalDAO.getAllByZooAndClass(zoo, animalClass);

        verify(mockDBConnection, times(1)).getConnection();
        verify(mockConnection, times(1)).prepareStatement(SELECT_ALL_ANIMAL_BY_ZOO_AND_CLASS.QUERY);
        verify(mockPrStatement, times(2)).setLong(anyInt(), anyInt());
        verify(mockPrStatement, times(1)).executeQuery();

        verify(mockResultSet, times(2)).next();
        verify(mockResultSet, times(1)).getLong(1);
        verify(mockResultSet, times(4)).getString(anyInt());

        verify(mockPrStatement, times(1)).close();
    }

    @Test(expected = DAOException.class)
    public void getAllByZooAndClassPrStatementException() throws Exception {
        when(mockConnection.prepareStatement(SELECT_ALL_ANIMAL_BY_ZOO_AND_CLASS.QUERY)).thenThrow(new SQLException());

        try {
            animalDAO.getAllByZooAndClass(zoo, animalClass);
        } catch (DAOException e) {
            verify(mockDBConnection, times(1)).getConnection();
            verify(mockConnection, times(1)).prepareStatement(SELECT_ALL_ANIMAL_BY_ZOO_AND_CLASS.QUERY);
            verify(mockPrStatement, times(0)).setLong(anyInt(), anyInt());
            verify(mockPrStatement, times(0)).executeQuery();

            verify(mockResultSet, times(0)).next();
            verify(mockResultSet, times(0)).getLong(1);
            verify(mockResultSet, times(0)).getString(anyInt());

            verify(mockPrStatement, times(0)).close();
            throw e;
        }
    }

    @Test(expected = DAOException.class)
    public void getAllByZooAndClassResultSetException() throws Exception {
        when(mockResultSet.getString(2)).thenThrow(new SQLException());
        ;

        try {
            animalDAO.getAllByZooAndClass(zoo, animalClass);
        } catch (DAOException e) {
            verify(mockDBConnection, times(1)).getConnection();
            verify(mockConnection, times(1)).prepareStatement(SELECT_ALL_ANIMAL_BY_ZOO_AND_CLASS.QUERY);
            verify(mockPrStatement, times(2)).setLong(anyInt(), anyInt());
            verify(mockPrStatement, times(1)).executeQuery();

            verify(mockResultSet, times(1)).next();
            verify(mockResultSet, times(1)).getLong(1);
            verify(mockResultSet, times(1)).getString(anyInt());

            verify(mockPrStatement, times(1)).close();
            throw e;
        }
    }
}