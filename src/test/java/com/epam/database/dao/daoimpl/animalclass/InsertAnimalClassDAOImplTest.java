package com.epam.database.dao.daoimpl.animalclass;

import com.epam.database.dao.daoimpl.AnimalClassDAOImpl;
import com.epam.database.dao.daoimpl.DAOMockTest;
import com.epam.database.exception.DAOException;
import com.epam.model.AnimalClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.SQLException;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class InsertAnimalClassDAOImplTest extends DAOMockTest {

    @Before
    public void setUp() throws Exception {
        animalClassDAO = new AnimalClassDAOImpl(mockDBConnection);

        when(mockDBConnection.getConnection()).thenReturn(mockConnection);
        when(mockConnection.prepareStatement(anyString())).thenReturn(mockPrStatement);

        doNothing().when(mockPrStatement).setString(anyInt(), anyString());
        when(mockPrStatement.executeUpdate()).thenReturn(1);
    }

    @Test
    public void insertNoExceptions() throws Exception {
        animalClassDAO.insert(new AnimalClass());

        verify(mockDBConnection, times(1)).getConnection();
        verify(mockConnection, times(1)).prepareStatement(anyString());
        verify(mockPrStatement, times(1)).setString(anyInt(), anyString());
        verify(mockPrStatement, times(1)).executeUpdate();
    }

    @Test(expected = DAOException.class)
    public void insertPrStatementExceptions() throws Exception {
        when(mockConnection.prepareStatement(anyString())).thenThrow(new SQLException());

        try {
            animalClassDAO.insert(new AnimalClass());
        } catch (DAOException e) {
            verify(mockDBConnection, times(1)).getConnection();
            verify(mockConnection, times(1)).prepareStatement(anyString());
            verify(mockPrStatement, times(0)).setString(anyInt(), anyString());
            verify(mockPrStatement, times(0)).executeUpdate();
            throw e;
        }
    }

    @Test(expected = DAOException.class)
    public void insertExecuteUpdateExceptions() throws Exception {
        when(mockPrStatement.executeUpdate()).thenThrow(new SQLException());

        try {
            animalClassDAO.insert(new AnimalClass());
        } catch (DAOException e) {
            verify(mockDBConnection, times(1)).getConnection();
            verify(mockConnection, times(1)).prepareStatement(anyString());
            verify(mockPrStatement, times(1)).setString(anyInt(), anyString());
            verify(mockPrStatement, times(1)).executeUpdate();
            throw e;
        }
    }

}