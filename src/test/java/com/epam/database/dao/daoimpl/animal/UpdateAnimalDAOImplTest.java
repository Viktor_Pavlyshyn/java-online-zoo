package com.epam.database.dao.daoimpl.animal;

import com.epam.database.dao.daoimpl.AnimalDAOImpl;
import com.epam.database.dao.daoimpl.DAOMockTest;
import com.epam.database.exception.DAOException;
import com.epam.model.Animal;
import com.epam.model.AnimalClass;
import com.epam.model.Zoo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.SQLException;

import static com.epam.database.config.SQLProp.UPDATE_ANIMAL;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UpdateAnimalDAOImplTest extends DAOMockTest {

    @Before
    public void setUp() throws Exception {
        animalDAO = new AnimalDAOImpl(mockDBConnection);
        animal = new Animal((long) 1, new Zoo((long) 1, "test-zoo"),
                new AnimalClass((long) 1, "test-class"), "test-name", "test-app");

        when(mockDBConnection.getConnection()).thenReturn(mockConnection);
        when(mockConnection.prepareStatement(UPDATE_ANIMAL.QUERY)).thenReturn(mockPrStatement);

        doNothing().when(mockPrStatement).setLong(anyInt(), anyInt());
        doNothing().when(mockPrStatement).setString(anyInt(), anyString());
    }

    @Test
    public void updateNoException() throws Exception {
        animalDAO.update(animal);

        verify(mockDBConnection, times(1)).getConnection();
        verify(mockConnection, times(1)).prepareStatement(UPDATE_ANIMAL.QUERY);

        verify(mockPrStatement, times(2)).setString(anyInt(), anyString());
        verify(mockPrStatement, times(3)).setLong(anyInt(), anyInt());

        verify(mockPrStatement, times(1)).executeUpdate();
    }

    @Test(expected = DAOException.class)
    public void updatePrStatementException() throws Exception {
        doThrow(new SQLException()).when(mockPrStatement).setString(eq(3), anyString());

        try {
            animalDAO.update(animal);
        } catch (DAOException e) {
            verify(mockDBConnection, times(1)).getConnection();
            verify(mockConnection, times(1)).prepareStatement(UPDATE_ANIMAL.QUERY);

            verify(mockPrStatement, times(1)).setString(anyInt(), anyString());
            verify(mockPrStatement, times(2)).setLong(anyInt(), anyInt());

            verify(mockPrStatement, times(0)).executeUpdate();
            throw e;
        }
    }

}