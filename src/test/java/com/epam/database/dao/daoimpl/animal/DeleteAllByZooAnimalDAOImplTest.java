package com.epam.database.dao.daoimpl.animal;

import com.epam.database.dao.daoimpl.AnimalDAOImpl;
import com.epam.database.dao.daoimpl.DAOMockTest;
import com.epam.database.exception.DAOException;
import com.epam.model.Zoo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.SQLException;

import static com.epam.database.config.SQLProp.DELETE_ANIMAL_BY_ZOO;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DeleteAllByZooAnimalDAOImplTest extends DAOMockTest {

    @Before
    public void setUp() throws Exception {
        animalDAO = new AnimalDAOImpl(mockDBConnection);
        zoo = new Zoo((long) 1, "test-zoo");

        when(mockDBConnection.getConnection()).thenReturn(mockConnection);
        when(mockConnection.prepareStatement(DELETE_ANIMAL_BY_ZOO.QUERY)).thenReturn(mockPrStatement);

        doNothing().when(mockPrStatement).setLong(eq(1), anyInt());

    }

    @Test
    public void deleteAllByZooNoException() throws Exception {
        animalDAO.deleteAllByZoo(zoo);

        verify(mockDBConnection, times(1)).getConnection();
        verify(mockConnection, times(1)).prepareStatement(DELETE_ANIMAL_BY_ZOO.QUERY);

        verify(mockPrStatement, times(1)).setLong(eq(1), anyInt());

        verify(mockPrStatement, times(1)).executeUpdate();
    }

    @Test(expected = DAOException.class)
    public void deleteAllByZooPrStatementException() throws Exception {
        doThrow(new SQLException()).when(mockPrStatement).setLong(eq(1), anyInt());

        try {
            animalDAO.deleteAllByZoo(zoo);
        } catch (DAOException e) {

            verify(mockDBConnection, times(1)).getConnection();
            verify(mockConnection, times(1)).prepareStatement(DELETE_ANIMAL_BY_ZOO.QUERY);

            verify(mockPrStatement, times(1)).setLong(eq(1), anyInt());

            verify(mockPrStatement, times(0)).executeUpdate();
            throw e;
        }
    }
}