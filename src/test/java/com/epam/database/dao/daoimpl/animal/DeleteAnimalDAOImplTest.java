package com.epam.database.dao.daoimpl.animal;

import com.epam.database.dao.daoimpl.AnimalDAOImpl;
import com.epam.database.dao.daoimpl.DAOMockTest;
import com.epam.database.exception.DAOException;
import com.epam.model.Animal;
import com.epam.model.AnimalClass;
import com.epam.model.Zoo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.SQLException;

import static com.epam.database.config.SQLProp.DELETE_ANIMAL;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DeleteAnimalDAOImplTest extends DAOMockTest {

    @Before
    public void setUp() throws Exception {
        animalDAO = new AnimalDAOImpl(mockDBConnection);
        animal = new Animal((long) 1, new Zoo((long) 1, "test-zoo"),
                new AnimalClass((long) 1, "test-class"), "test-name", "test-app");

        when(mockDBConnection.getConnection()).thenReturn(mockConnection);
        when(mockConnection.prepareStatement(DELETE_ANIMAL.QUERY)).thenReturn(mockPrStatement);

        doNothing().when(mockPrStatement).setLong(eq(1), anyInt());
    }

    @Test
    public void deleteNoException() throws Exception {
        animalDAO.delete(animal);

        verify(mockDBConnection, times(1)).getConnection();
        verify(mockConnection, times(1)).prepareStatement(DELETE_ANIMAL.QUERY);

        verify(mockPrStatement, times(1)).setLong(eq(1), anyInt());

        verify(mockPrStatement, times(1)).executeUpdate();
    }

    @Test(expected = DAOException.class)
    public void deletePrStatementException() throws Exception {
        doThrow(new SQLException()).when(mockPrStatement).setLong(eq(1), anyInt());

        try {
            animalDAO.delete(animal);
        } catch (DAOException e) {

            verify(mockDBConnection, times(1)).getConnection();
            verify(mockConnection, times(1)).prepareStatement(DELETE_ANIMAL.QUERY);

            verify(mockPrStatement, times(1)).setLong(eq(1), anyInt());

            verify(mockPrStatement, times(0)).executeUpdate();
            throw e;
        }
    }
}