package com.epam.database.dao.daoimpl.animalclass;

import com.epam.database.dao.daoimpl.AnimalClassDAOImpl;
import com.epam.database.dao.daoimpl.DAOMockTest;
import com.epam.database.exception.DAOException;
import com.epam.model.AnimalClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.SQLException;
import java.util.LinkedList;

import static com.epam.database.config.SQLProp.SELECT_ALL_ANIMAL_CLASS;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class GetAllAnimalClassDAOImplTest extends DAOMockTest {

    @Before
    public void setUp() throws Exception {
        animalClassDAO = new AnimalClassDAOImpl(mockDBConnection);
        listAnimalClasses = new LinkedList<>();
        listAnimalClasses.add(new AnimalClass((long) 1, "test-animalClass"));

        when(mockDBConnection.getConnection()).thenReturn(mockConnection);
        when(mockConnection.createStatement()).thenReturn(mockStatement);
        when(mockStatement.executeQuery(SELECT_ALL_ANIMAL_CLASS.QUERY)).thenReturn(mockResultSet);

        when(mockResultSet.next()).thenReturn(true).thenReturn(false);
        when(mockResultSet.getLong(1)).thenReturn((long) 1);
        when(mockResultSet.getString(2)).thenReturn("test-animalClass");
    }

    @Test
    public void getAllNoException() throws Exception {
        animalClassDAO.getAll();

        verify(mockDBConnection, times(1)).getConnection();
        verify(mockConnection, times(1)).createStatement();
        verify(mockStatement, times(1)).executeQuery(SELECT_ALL_ANIMAL_CLASS.QUERY);

        verify(mockResultSet, times(2)).next();
        verify(mockResultSet, times(1)).getLong(1);
        verify(mockResultSet, times(1)).getString(2);
    }

    @Test(expected = DAOException.class)
    public void getAllResultSetException() throws Exception {
        when(mockResultSet.getLong(1)).thenThrow(new SQLException());

        try {
            animalClassDAO.getAll();
        } catch (DAOException e) {
            verify(mockDBConnection, times(1)).getConnection();
            verify(mockConnection, times(1)).createStatement();
            verify(mockStatement, times(1)).executeQuery(SELECT_ALL_ANIMAL_CLASS.QUERY);

            verify(mockResultSet, times(1)).next();
            verify(mockResultSet, times(1)).getLong(1);
            verify(mockResultSet, times(0)).getString(2);
            throw e;
        }
    }
}