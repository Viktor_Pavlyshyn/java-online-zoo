package com.epam.database.dao.daoimpl.animal;

import com.epam.database.dao.daoimpl.AnimalDAOImpl;
import com.epam.database.dao.daoimpl.DAOMockTest;
import com.epam.database.exception.DAOException;
import com.epam.model.Zoo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.SQLException;

import static com.epam.database.config.SQLProp.SELECT_ALL_CLASS_BY_ZOO;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class GetAllClassIdByZooAnimalDAOImplTest extends DAOMockTest {

    @Before
    public void setUp() throws Exception {
        animalDAO = new AnimalDAOImpl(mockDBConnection);
        zoo = new Zoo((long) 1, "test-zoo");

        when(mockDBConnection.getConnection()).thenReturn(mockConnection);
        when(mockConnection.prepareStatement(SELECT_ALL_CLASS_BY_ZOO.QUERY)).thenReturn(mockPrStatement);
        doNothing().when(mockPrStatement).setLong(anyInt(), anyInt());
        when(mockPrStatement.executeQuery()).thenReturn(mockResultSet);

        when(mockResultSet.next()).thenReturn(true).thenReturn(false);

        when(mockResultSet.getLong(1)).thenReturn((long) 1);
    }

    @Test
    public void getAllClassIdByZooNoException() throws Exception {
        animalDAO.getAllClassIdByZoo(zoo);

        verify(mockDBConnection, times(1)).getConnection();
        verify(mockConnection, times(1)).prepareStatement(SELECT_ALL_CLASS_BY_ZOO.QUERY);
        verify(mockPrStatement, times(1)).setLong(anyInt(), anyInt());
        verify(mockPrStatement, times(1)).executeQuery();

        verify(mockResultSet, times(2)).next();
        verify(mockResultSet, times(1)).getLong(1);

        verify(mockPrStatement, times(1)).close();
    }

    @Test(expected = DAOException.class)
    public void getAllClassIdByZooPrStatementException() throws Exception {
        when(mockConnection.prepareStatement(SELECT_ALL_CLASS_BY_ZOO.QUERY)).thenThrow(new SQLException());

        try {
            animalDAO.getAllClassIdByZoo(zoo);
        } catch (DAOException e) {
            verify(mockDBConnection, times(1)).getConnection();
            verify(mockConnection, times(1)).prepareStatement(SELECT_ALL_CLASS_BY_ZOO.QUERY);
            verify(mockPrStatement, times(0)).setLong(anyInt(), anyInt());
            verify(mockPrStatement, times(0)).executeQuery();

            verify(mockResultSet, times(0)).next();
            verify(mockResultSet, times(0)).getLong(1);

            verify(mockPrStatement, times(0)).close();
            throw e;
        }
    }

    @Test(expected = DAOException.class)
    public void getAllClassIdByZooResultSetException() throws Exception {
        when(mockResultSet.getLong(1)).thenThrow(new SQLException());
        ;

        try {
            animalDAO.getAllClassIdByZoo(zoo);
        } catch (DAOException e) {
            verify(mockDBConnection, times(1)).getConnection();
            verify(mockConnection, times(1)).prepareStatement(SELECT_ALL_CLASS_BY_ZOO.QUERY);
            verify(mockPrStatement, times(1)).setLong(anyInt(), anyInt());
            verify(mockPrStatement, times(1)).executeQuery();

            verify(mockResultSet, times(1)).next();
            verify(mockResultSet, times(1)).getLong(1);

            verify(mockPrStatement, times(1)).close();
            throw e;
        }
    }
}