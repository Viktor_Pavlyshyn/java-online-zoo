package com.epam.servlet;

import com.epam.database.dao.AnimalClassDAO;
import com.epam.database.dao.AnimalDAO;
import com.epam.database.dao.ZooDAO;
import com.epam.model.Animal;
import com.epam.model.AnimalClass;
import com.epam.model.Zoo;
import org.mockito.Mock;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.LinkedList;
import java.util.Set;

public abstract class ServletMockTest {
    @Mock
    public ServletContext context;
    @Mock
    public HttpServletRequest request;
    @Mock
    public HttpServletResponse response;
    @Mock
    protected RequestDispatcher dispatcher;
    @Mock
    public ZooDAO zooDAO;
    @Mock
    public AnimalClassDAO animalClassDAO;
    @Mock
    public AnimalDAO animalDAO;
    protected LinkedList<Zoo> listZoos;
    protected LinkedList<AnimalClass> listClasses;
    protected LinkedList<Animal> listAnimals;
    protected Set<Long> setId;
    public Zoo zoo;
    protected AnimalClass animalClass;
    protected Animal animal;


}
