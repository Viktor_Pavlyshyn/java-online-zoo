package com.epam.servlet;

import com.epam.database.dao.AnimalClassDAO;
import com.epam.database.dao.ZooDAO;
import com.epam.database.dao.daoimpl.AnimalDAOImpl;
import com.epam.database.utils.DBTable;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ContextListenerTest {
    private ContextListener contextListener;

    @Mock
    private ServletContextEvent servletContextEvent;
    @Mock
    private ServletContext servletContext;
    @Mock
    private DBTable dbTable;

    @Before
    public void setUp() throws Exception {
        contextListener = new ContextListener();

        doNothing().when(dbTable).createTable(anyString());

        when(servletContextEvent.getServletContext()).thenReturn(servletContext);
    }

    @Test
    public void contextInitialized() throws Exception {
        contextListener.setDbTable(dbTable);
        contextListener.contextInitialized(servletContextEvent);

        verify(servletContextEvent, times(1)).getServletContext();

        verify(dbTable, times(3)).createTable(anyString());

        verify(servletContext, times(1)).setAttribute(eq("animalDAO"), any(AnimalDAOImpl.class));
        verify(servletContext, times(1)).setAttribute(eq("animalClassDAO"), any(AnimalClassDAO.class));
        verify(servletContext, times(1)).setAttribute(eq("zooDAO"), any(ZooDAO.class));
    }

}