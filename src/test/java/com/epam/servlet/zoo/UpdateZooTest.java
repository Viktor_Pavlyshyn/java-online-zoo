package com.epam.servlet.zoo;

import com.epam.database.exception.DAOException;
import com.epam.model.Zoo;
import com.epam.servlet.ServletMockTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import static com.epam.servlet.config.PathJSP.UPDATE_ZOO_JSP;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UpdateZooTest extends ServletMockTest {
    private UpdateZoo updateZoo;

    @Before
    public void setUp() throws Exception {
        updateZoo = new UpdateZoo() {
            @Override
            public ServletContext getServletContext() {
                return context;
            }
        };
        zoo = new Zoo((long) 1, "zoo-test");

        when(updateZoo.getServletContext().getAttribute("zooDAO")).thenReturn(zooDAO);
        when(request.getParameter("idZoo")).thenReturn(String.valueOf(1));
        when(request.getParameter("nameZoo")).thenReturn("zoo-test");
        when(zooDAO.getById(anyLong())).thenReturn(zoo);
        doNothing().when(zooDAO).update(anyObject());
        when(request.getRequestDispatcher(UPDATE_ZOO_JSP.PATH)).thenReturn(dispatcher);
    }

    @Test
    public void initNoException() throws Exception {
        updateZoo.init();

        verify(updateZoo.getServletContext(), times(1)).getAttribute("zooDAO");
    }

    @Test
    public void doGetNoException() throws Exception {
        updateZoo.init();
        updateZoo.doGet(request, response);

        verify(request, times(1)).getParameter("idZoo");
        verify(zooDAO, times(1)).getById((long) 1);
        verify(request, times(1)).setAttribute("zoo", zoo);
        verify(request, times(1)).getRequestDispatcher(UPDATE_ZOO_JSP.PATH);
        verify(dispatcher, times(1)).forward(request, response);
    }

    @Test
    public void doGetFalseId() throws Exception {
        when(request.getParameter("idZoo")).thenReturn("false id");

        updateZoo.init();
        updateZoo.doGet(request, response);

        verify(request, times(1)).getParameter("idZoo");
        verify(zooDAO, times(0)).getById((long) 1);
        verify(request, times(0)).setAttribute("zoo", zoo);
        verify(response, times(1)).sendRedirect(String.format("%s/", request.getContextPath()));
        verify(request, times(0)).getRequestDispatcher(UPDATE_ZOO_JSP.PATH);
        verify(dispatcher, times(0)).forward(request, response);
    }

    @Test(expected = ServletException.class)
    public void doGetByIdException() throws Exception {
        doThrow(new DAOException()).when(zooDAO).getById(anyLong());
        try {
            updateZoo.init();
            updateZoo.doGet(request, response);
        } catch (ServletException e) {
            verify(request, times(1)).getParameter("idZoo");
            verify(zooDAO, times(1)).getById((long) 1);
            verify(request, times(0)).setAttribute("zoo", zoo);
            verify(request, times(0)).getRequestDispatcher(UPDATE_ZOO_JSP.PATH);
            verify(dispatcher, times(0)).forward(request, response);
            throw e;
        }
    }

    @Test
    public void doPostNoException() throws Exception {
        updateZoo.init();
        updateZoo.doPost(request, response);

        verify(request, times(1)).getParameter("idZoo");
        verify(request, times(1)).getParameter("nameZoo");
        verify(zooDAO, times(1)).update(anyObject());
        verify(response, times(0)).sendError(eq(HttpServletResponse.SC_BAD_REQUEST), anyString());
        verify(response, times(1)).sendRedirect(String.format("%s/", request.getContextPath()));

    }

    @Test
    public void doPostInvalidParam() throws Exception {
        when(request.getParameter("nameZoo")).thenReturn("");

        updateZoo.init();
        updateZoo.doPost(request, response);

        verify(request, times(1)).getParameter("idZoo");
        verify(request, times(1)).getParameter("nameZoo");
        verify(zooDAO, times(0)).update(anyObject());
        verify(response, times(1)).sendError(eq(HttpServletResponse.SC_BAD_REQUEST), anyString());
        verify(response, times(0)).sendRedirect(String.format("%s/", request.getContextPath()));

    }

    @Test(expected = ServletException.class)
    public void doPostUpdateException() throws Exception {
        doThrow(new DAOException()).when(zooDAO).update(anyObject());
        try {
            updateZoo.init();
            updateZoo.doPost(request, response);
        } catch (ServletException e) {
            verify(request, times(1)).getParameter("idZoo");
            verify(request, times(1)).getParameter("nameZoo");
            verify(zooDAO, times(1)).update(anyObject());
            verify(response, times(0)).sendError(eq(HttpServletResponse.SC_BAD_REQUEST), anyString());
            verify(response, times(0)).sendRedirect(String.format("%s/", request.getContextPath()));
            throw e;
        }
    }
}