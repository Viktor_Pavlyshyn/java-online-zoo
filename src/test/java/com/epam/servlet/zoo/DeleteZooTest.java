package com.epam.servlet.zoo;

import com.epam.database.exception.DAOException;
import com.epam.servlet.ServletMockTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DeleteZooTest extends ServletMockTest {
    private DeleteZoo deleteZoo;

    @Before
    public void setUp() throws Exception {
        deleteZoo = new DeleteZoo() {
            public ServletContext getServletContext() {
                return context;
            }
        };

        when(deleteZoo.getServletContext().getAttribute("animalDAO")).thenReturn(animalDAO);
        when(deleteZoo.getServletContext().getAttribute("zooDAO")).thenReturn(zooDAO);
        when(request.getParameter("idZoo")).thenReturn(String.valueOf(1));
        doNothing().when(animalDAO).deleteAllByZoo(anyObject());
        doNothing().when(zooDAO).delete(anyObject());
    }

    @Test
    public void initNoException() throws Exception {
        deleteZoo.init();

        verify(deleteZoo.getServletContext(), times(1)).getAttribute("animalDAO");
        verify(deleteZoo.getServletContext(), times(1)).getAttribute("zooDAO");
    }

    @Test
    public void doPostNoException() throws Exception {
        deleteZoo.init();
        deleteZoo.doPost(request, response);

        verify(request, times(1)).getParameter("idZoo");
        verify(animalDAO, times(1)).deleteAllByZoo(anyObject());
        verify(zooDAO, times(1)).delete(anyObject());
        verify(response, times(1)).sendRedirect(String.format("%s/", request.getContextPath()));
    }

    @Test
    public void doPostInvalidId() throws Exception {
        when(request.getParameter("idZoo")).thenReturn("false id");
        deleteZoo.init();
        deleteZoo.doPost(request, response);

        verify(request, times(1)).getParameter("idZoo");
        verify(animalDAO, times(0)).deleteAllByZoo(anyObject());
        verify(zooDAO, times(0)).delete(anyObject());
        verify(response, times(1)).sendRedirect(String.format("%s/", request.getContextPath()));
    }

    @Test(expected = ServletException.class)
    public void doPostDeleteAllAnimalByZooException() throws Exception {
        doThrow(new DAOException()).when(animalDAO).deleteAllByZoo(anyObject());

        try {
            deleteZoo.init();
            deleteZoo.doPost(request, response);
        } catch (ServletException e) {
            verify(request, times(1)).getParameter("idZoo");
            verify(animalDAO, times(1)).deleteAllByZoo(anyObject());
            verify(zooDAO, times(0)).delete(anyObject());
            verify(response, times(0)).sendRedirect(String.format("%s/", request.getContextPath()));
            throw e;
        }
    }

    @Test(expected = ServletException.class)
    public void doPostDeleteException() throws Exception {
        doThrow(new DAOException()).when(zooDAO).delete(anyObject());
        try {
            deleteZoo.init();
            deleteZoo.doPost(request, response);
        } catch (ServletException e) {
            verify(request, times(1)).getParameter("idZoo");
            verify(animalDAO, times(1)).deleteAllByZoo(anyObject());
            verify(zooDAO, times(1)).delete(anyObject());
            verify(response, times(0)).sendRedirect(String.format("%s/", request.getContextPath()));
            throw e;
        }
    }
}