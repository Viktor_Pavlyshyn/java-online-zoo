package com.epam.servlet.zoo;

import com.epam.database.exception.DAOException;
import com.epam.servlet.ServletMockTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AddZooTest extends ServletMockTest {
    private AddZoo addZoo;

    @Before
    public void setUp() throws Exception {
        addZoo = new AddZoo() {
            public ServletContext getServletContext() {
                return context;
            }
        };

        when(addZoo.getServletContext().getAttribute("zooDAO")).thenReturn(zooDAO);
        when(request.getParameter("nameZoo")).thenReturn("zoo-test");
        doNothing().when(zooDAO).insert(anyObject());
        doNothing().when(response).sendError(eq(HttpServletResponse.SC_BAD_REQUEST), anyString());

    }

    @Test
    public void initNoException() throws Exception {
        addZoo.init();

        verify(addZoo.getServletContext(), times(1)).getAttribute("zooDAO");
    }

    @Test
    public void doPostNoException() throws Exception {
        addZoo.init();
        addZoo.doPost(request, response);

        verify(request, times(1)).getParameter("nameZoo");
        verify(zooDAO, times(1)).insert(anyObject());
        verify(response, times(0)).sendError(eq(HttpServletResponse.SC_BAD_REQUEST), anyString());
        verify(response, times(1)).sendRedirect(String.format("%s/", request.getContextPath()));
    }

    @Test
    public void doPostInvalidParam() throws Exception {
        when(request.getParameter("nameZoo")).thenReturn("");

        addZoo.init();
        addZoo.doPost(request, response);
        verify(request, times(1)).getParameter("nameZoo");
        verify(zooDAO, times(0)).insert(anyObject());
        verify(response, times(1)).sendError(eq(HttpServletResponse.SC_BAD_REQUEST), anyString());

        verify(response, times(1)).sendRedirect(String.format("%s/", request.getContextPath()));

    }

    @Test(expected = ServletException.class)
    public void doPostException() throws Exception {
        doThrow(new DAOException()).when(zooDAO).insert(anyObject());

        try {
            addZoo.init();
            addZoo.doPost(request, response);
        } catch (ServletException e) {
            verify(request, times(1)).getParameter("nameZoo");
            verify(zooDAO, times(1)).insert(anyObject());
            verify(response, times(0)).sendError(eq(HttpServletResponse.SC_BAD_REQUEST), anyString());
            verify(response, times(0)).sendRedirect(String.format("%s/", request.getContextPath()));
            throw e;
        }
    }
}