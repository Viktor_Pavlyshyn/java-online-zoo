package com.epam.servlet.zoo;

import com.epam.database.exception.DAOException;
import com.epam.database.exception.DBConnectionException;
import com.epam.servlet.ServletMockTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import static com.epam.servlet.config.PathJSP.ZOO_JSP;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ServletZooTest extends ServletMockTest {
    private ServletZoo servletZoo;

    @Before
    public void setUp() throws DBConnectionException, DAOException {
        servletZoo = new ServletZoo() {
            public ServletContext getServletContext() {
                return context;
            }
        };

        when(servletZoo.getServletContext().getAttribute("zooDAO")).thenReturn(zooDAO);
        when(servletZoo.getServletContext().getAttribute("animalClassDAO")).thenReturn(animalClassDAO);

        when(zooDAO.getAll()).thenReturn(listZoos);
        when(animalClassDAO.getAll()).thenReturn(listClasses);
        doNothing().when(request).setAttribute("zoos", listZoos);
        doNothing().when(request).setAttribute("animalClasses", listClasses);

        when(request.getRequestDispatcher(ZOO_JSP.PATH)).thenReturn(dispatcher);
    }

    @Test
    public void initNoException() throws Exception {
        servletZoo.init();

        verify(servletZoo.getServletContext(), times(1)).getAttribute("animalClassDAO");
        verify(servletZoo.getServletContext(), times(1)).getAttribute("zooDAO");
    }

    @Test
    public void doGetNoException() throws Exception {
        servletZoo.init();
        servletZoo.doGet(request, response);

        verify(zooDAO, times(1)).getAll();
        verify(animalClassDAO, times(1)).getAll();
        verify(request, times(1)).setAttribute("zoos", listZoos);
        verify(request, times(1)).setAttribute("animalClasses", listClasses);
        verify(request, times(1)).getRequestDispatcher(ZOO_JSP.PATH);
        verify(dispatcher, times(1)).forward(request, response);
    }

    @Test(expected = ServletException.class)
    public void doGetAllZoosException() throws Exception {
        doThrow(new DAOException()).when(zooDAO).getAll();
        try {
            servletZoo.init();
            servletZoo.doGet(request, response);
        } catch (ServletException e) {
            verify(zooDAO, times(1)).getAll();
            verify(animalClassDAO, times(0)).getAll();
            verify(request, times(0)).setAttribute("zoos", listZoos);
            verify(request, times(0)).setAttribute("animalClasses", listClasses);
            verify(request, times(0)).getRequestDispatcher(ZOO_JSP.PATH);
            verify(dispatcher, times(0)).forward(request, response);
            throw e;
        }
    }

    @Test(expected = ServletException.class)
    public void doGetAllClassesException() throws Exception {
        doThrow(new DAOException()).when(animalClassDAO).getAll();
        try {
            servletZoo.init();
            servletZoo.doGet(request, response);
        } catch (ServletException e) {
            verify(zooDAO, times(1)).getAll();
            verify(animalClassDAO, times(1)).getAll();
            verify(request, times(0)).setAttribute("zoos", listZoos);
            verify(request, times(0)).setAttribute("animalClasses", listClasses);
            verify(request, times(0)).getRequestDispatcher(ZOO_JSP.PATH);
            verify(dispatcher, times(0)).forward(request, response);
            throw e;
        }
    }
}