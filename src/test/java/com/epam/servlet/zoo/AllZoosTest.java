package com.epam.servlet.zoo;

import com.epam.database.exception.DAOException;
import com.epam.servlet.ServletMockTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import static com.epam.servlet.config.PathJSP.ALL_ZOOS_JSP;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AllZoosTest extends ServletMockTest {
    private AllZoos allZoos;

    @Before
    public void setUp() throws Exception {
        allZoos = new AllZoos() {
            public ServletContext getServletContext() {
                return context;
            }
        };

        when(allZoos.getServletContext().getAttribute("zooDAO")).thenReturn(zooDAO);

        when(zooDAO.getAll()).thenReturn(listZoos);
        doNothing().when(request).setAttribute("zoos", listZoos);

        when(request.getRequestDispatcher(ALL_ZOOS_JSP.PATH)).thenReturn(dispatcher);
    }

    @Test
    public void initNoException() throws Exception {
        allZoos.init();

        verify(allZoos.getServletContext(), times(1)).getAttribute("zooDAO");
    }

    @Test
    public void doGetNoException() throws Exception {
        allZoos.init();
        allZoos.doGet(request, response);

        verify(zooDAO, times(1)).getAll();
        verify(request, times(1)).setAttribute("zoos", listZoos);
        verify(request, times(1)).getRequestDispatcher(ALL_ZOOS_JSP.PATH);
        verify(dispatcher, times(1)).forward(request, response);

    }

    @Test(expected = ServletException.class)
    public void doGetAllZoosException() throws Exception {
        doThrow(new DAOException()).when(zooDAO).getAll();
        try {
            allZoos.init();
            allZoos.doGet(request, response);
        } catch (ServletException e) {
            verify(zooDAO, times(1)).getAll();
            verify(request, times(0)).setAttribute("zoos", listZoos);
            verify(request, times(0)).getRequestDispatcher(ALL_ZOOS_JSP.PATH);
            verify(dispatcher, times(0)).forward(request, response);
            throw e;
        }
    }
}