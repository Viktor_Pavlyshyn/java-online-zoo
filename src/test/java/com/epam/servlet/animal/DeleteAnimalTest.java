package com.epam.servlet.animal;

import com.epam.database.exception.DAOException;
import com.epam.servlet.ServletMockTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DeleteAnimalTest extends ServletMockTest {
    private DeleteAnimal deleteAnimal;

    @Before
    public void setUp() throws Exception {
        deleteAnimal = new DeleteAnimal() {
            @Override
            public ServletContext getServletContext() {
                return context;
            }
        };

        when(deleteAnimal.getServletContext().getAttribute("animalDAO")).thenReturn(animalDAO);
        when(request.getParameter("id")).thenReturn(String.valueOf(1));
        doNothing().when(animalDAO).delete(anyObject());
    }

    @Test
    public void initNoException() throws Exception {
        deleteAnimal.init();

        verify(deleteAnimal.getServletContext(), times(1)).getAttribute("animalDAO");
    }

    @Test
    public void doPostNoException() throws Exception {
        deleteAnimal.init();
        deleteAnimal.doPost(request, response);

        verify(request, times(1)).getParameter("id");
        verify(animalDAO, times(1)).delete(anyObject());
        verify(response, times(1)).sendRedirect(String.format("%s/animal", request.getContextPath()));
    }

    @Test
    public void doPostInvalidId() throws Exception {
        when(request.getParameter("id")).thenReturn("false id");

        deleteAnimal.init();
        deleteAnimal.doPost(request, response);

        verify(request, times(1)).getParameter("id");
        verify(animalDAO, times(0)).delete(anyObject());
        verify(response, times(1)).sendRedirect(String.format("%s/animal", request.getContextPath()));
    }

    @Test(expected = ServletException.class)
    public void doPostDeleteAnimalException() throws Exception {
        doThrow(new DAOException()).when(animalDAO).delete(anyObject());
        try {
            deleteAnimal.init();
            deleteAnimal.doPost(request, response);
        } catch (ServletException e) {
            verify(request, times(1)).getParameter("id");
            verify(animalDAO, times(1)).delete(anyObject());
            verify(response, times(0)).sendRedirect(String.format("%s/animal", request.getContextPath()));
            throw e;
        }
    }

}