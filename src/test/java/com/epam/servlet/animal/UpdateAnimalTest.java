package com.epam.servlet.animal;

import com.epam.database.exception.DAOException;
import com.epam.database.exception.DBConnectionException;
import com.epam.servlet.ServletMockTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import static com.epam.servlet.config.PathJSP.UPDATE_ANIMAL_JSP;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UpdateAnimalTest extends ServletMockTest {
    private UpdateAnimal updateAnimal;

    @Before
    public void setUp() throws DBConnectionException, DAOException {
        updateAnimal = new UpdateAnimal() {
            @Override
            public ServletContext getServletContext() {
                return context;
            }
        };
        // init
        when(updateAnimal.getServletContext().getAttribute("zooDAO")).thenReturn(zooDAO);
        when(updateAnimal.getServletContext().getAttribute("animalClassDAO")).thenReturn(animalClassDAO);
        when(updateAnimal.getServletContext().getAttribute("animalDAO")).thenReturn(animalDAO);
        //doGet
        when(request.getParameter("id")).thenReturn(String.valueOf(1));
        when(request.getParameter("nameZoo")).thenReturn("test-nameZoo");
        when(request.getParameter("type")).thenReturn("test-type");

        when(zooDAO.getAll()).thenReturn(listZoos);
        when(animalClassDAO.getAll()).thenReturn(listClasses);
        when(animalDAO.getById(anyLong())).thenReturn(animal);

        doNothing().when(request).setAttribute("zoos", listZoos);
        doNothing().when(request).setAttribute("animalClasses", listClasses);
        doNothing().when(request).setAttribute("animal", animal);

        doNothing().when(request).setAttribute("nameZoo", "test-nameZoo");
        doNothing().when(request).setAttribute("type", "test-type");

        when(request.getRequestDispatcher(UPDATE_ANIMAL_JSP.PATH)).thenReturn(dispatcher);
        //doPost
        when(request.getParameter("idZoo")).thenReturn(String.valueOf(1));
        when(request.getParameter("idType")).thenReturn(String.valueOf(1));
        when(request.getParameter("animalName")).thenReturn("test-animalName");
        when(request.getParameter("appearance")).thenReturn("test-appearance");

        doNothing().when(animalDAO).update(animal);
    }

    @Test
    public void initNoException() throws Exception {
        updateAnimal.init();

        verify(updateAnimal.getServletContext(), times(1)).getAttribute("zooDAO");
        verify(updateAnimal.getServletContext(), times(1)).getAttribute("animalClassDAO");
        verify(updateAnimal.getServletContext(), times(1)).getAttribute("animalDAO");
    }

    @Test
    public void doGetNoException() throws Exception {
        updateAnimal.init();
        updateAnimal.doGet(request, response);

        verify(request, times(1)).getParameter("id");
        verify(request, times(1)).getParameter("nameZoo");
        verify(request, times(1)).getParameter("type");

        verify(zooDAO, times(1)).getAll();
        verify(animalClassDAO, times(1)).getAll();
        verify(animalDAO, times(1)).getById(anyLong());

        verify(request, times(1)).setAttribute("zoos", listZoos);
        verify(request, times(1)).setAttribute("animalClasses", listClasses);
        verify(request, times(1)).setAttribute("animal", animal);

        verify(response, times(0)).sendRedirect(String.format("%s/animal", request.getContextPath()));

        verify(request, times(1)).setAttribute("nameZoo", "test-nameZoo");
        verify(request, times(1)).setAttribute("type", "test-type");
        verify(request, times(1)).getRequestDispatcher(UPDATE_ANIMAL_JSP.PATH);
        verify(dispatcher, times(1)).forward(request, response);
    }

    @Test
    public void doGetInvalidId() throws Exception {
        when(request.getParameter("id")).thenReturn("test-id");

        updateAnimal.init();
        updateAnimal.doGet(request, response);

        verify(request, times(1)).getParameter("id");
        verify(request, times(1)).getParameter("nameZoo");
        verify(request, times(1)).getParameter("type");

        verify(zooDAO, times(0)).getAll();
        verify(animalClassDAO, times(0)).getAll();
        verify(animalDAO, times(0)).getById(anyLong());

        verify(request, times(0)).setAttribute("zoos", listZoos);
        verify(request, times(0)).setAttribute("animalClasses", listClasses);
        verify(request, times(0)).setAttribute("animal", animal);

        verify(response, times(1)).sendRedirect(String.format("%s/animal", request.getContextPath()));

        verify(request, times(0)).setAttribute("nameZoo", "test-nameZoo");
        verify(request, times(0)).setAttribute("type", "test-type");
        verify(request, times(0)).getRequestDispatcher(UPDATE_ANIMAL_JSP.PATH);
        verify(dispatcher, times(0)).forward(request, response);
    }

    @Test(expected = ServletException.class)
    public void doGetException() throws Exception {
        when(animalClassDAO.getAll()).thenThrow(new DAOException());
        try {
            updateAnimal.init();
            updateAnimal.doGet(request, response);
        } catch (ServletException e) {
            verify(request, times(1)).getParameter("id");
            verify(request, times(1)).getParameter("nameZoo");
            verify(request, times(1)).getParameter("type");

            verify(zooDAO, times(1)).getAll();
            verify(animalClassDAO, times(1)).getAll();
            verify(animalDAO, times(0)).getById(anyLong());

            verify(request, times(0)).setAttribute("zoos", listZoos);
            verify(request, times(0)).setAttribute("animalClasses", listClasses);
            verify(request, times(0)).setAttribute("animal", animal);

            verify(response, times(0)).sendRedirect(String.format("%s/animal", request.getContextPath()));

            verify(request, times(0)).setAttribute("nameZoo", "test-nameZoo");
            verify(request, times(0)).setAttribute("type", "test-type");
            verify(request, times(0)).getRequestDispatcher(UPDATE_ANIMAL_JSP.PATH);
            verify(dispatcher, times(0)).forward(request, response);
            throw e;
        }
    }

    @Test
    public void doPostNoException() throws Exception {
        updateAnimal.init();
        updateAnimal.doPost(request, response);

        verify(request, times(1)).getParameter("id");
        verify(request, times(1)).getParameter("idZoo");
        verify(request, times(1)).getParameter("idType");
        verify(request, times(1)).getParameter("animalName");
        verify(request, times(1)).getParameter("appearance");
        verify(response, times(0)).sendError(eq(HttpServletResponse.SC_BAD_REQUEST), anyString());
        verify(animalDAO, times(1)).update(anyObject());
        verify(response, times(1)).sendRedirect(String.format("%s/animal", request.getContextPath()));
    }

    @Test
    public void doPostInvalidParam() throws Exception {
        when(request.getParameter("animalName")).thenReturn("");

        updateAnimal.init();
        updateAnimal.doPost(request, response);

        verify(request, times(1)).getParameter("id");
        verify(request, times(1)).getParameter("idZoo");
        verify(request, times(1)).getParameter("idType");
        verify(request, times(1)).getParameter("animalName");
        verify(request, times(1)).getParameter("appearance");
        verify(response, times(1)).sendError(eq(HttpServletResponse.SC_BAD_REQUEST), anyString());
        verify(animalDAO, times(0)).update(anyObject());
        verify(response, times(0)).sendRedirect(String.format("%s/animal", request.getContextPath()));
    }

    @Test(expected = ServletException.class)
    public void doPostUpdateAnimalException() throws Exception {
        doThrow(new DAOException()).when(animalDAO).update(anyObject());
        try {
            updateAnimal.init();
            updateAnimal.doPost(request, response);
        } catch (ServletException e) {
            verify(request, times(1)).getParameter("id");
            verify(request, times(1)).getParameter("idZoo");
            verify(request, times(1)).getParameter("idType");
            verify(request, times(1)).getParameter("animalName");
            verify(request, times(1)).getParameter("appearance");
            verify(animalDAO, times(1)).update(anyObject());
            verify(response, times(0)).sendError(eq(HttpServletResponse.SC_BAD_REQUEST), anyString());
            verify(response, times(0)).sendRedirect(String.format("%s/animal", request.getContextPath()));
            throw e;
        }
    }

}