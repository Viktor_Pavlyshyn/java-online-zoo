package com.epam.servlet.animal;

import com.epam.database.exception.DAOException;
import com.epam.servlet.ServletMockTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DeleteAnimalClassFromAnimalTest extends ServletMockTest {
    private DeleteAnimalClassFromAnimal deleteAnimalClassFromAnimal;

    @Before
    public void setUp() throws Exception {
        deleteAnimalClassFromAnimal = new DeleteAnimalClassFromAnimal() {
            @Override
            public ServletContext getServletContext() {
                return context;
            }
        };
        when(deleteAnimalClassFromAnimal.getServletContext().getAttribute("animalDAO")).thenReturn(animalDAO);

        when(request.getParameter("id")).thenReturn(String.valueOf(1));
        when(request.getParameter("type")).thenReturn("test-type");
        when(request.getParameter("idZoo")).thenReturn(String.valueOf(1));
        when(request.getParameter("nameZoo")).thenReturn("test-nameZoo");

        doNothing().when(animalDAO).deleteByClassAndZoo(zoo, animalClass);
    }

    @Test
    public void initNoException() throws Exception {
        deleteAnimalClassFromAnimal.init();

        verify(deleteAnimalClassFromAnimal.getServletContext(), times(1)).getAttribute("animalDAO");
    }

    @Test
    public void doPostNoException() throws Exception {
        deleteAnimalClassFromAnimal.init();
        deleteAnimalClassFromAnimal.doPost(request, response);

        verify(request, times(1)).getParameter("id");
        verify(request, times(1)).getParameter("type");
        verify(request, times(1)).getParameter("idZoo");
        verify(request, times(1)).getParameter("nameZoo");
        verify(animalDAO, times(1)).deleteByClassAndZoo(anyObject(), anyObject());
        verify(response, times(1)).sendRedirect(String.format("%s/animal_type", request.getContextPath()));
    }

    @Test
    public void doPostInvalidId() throws Exception {
        when(request.getParameter("idZoo")).thenReturn("test-id");

        deleteAnimalClassFromAnimal.init();
        deleteAnimalClassFromAnimal.doPost(request, response);

        verify(request, times(1)).getParameter("id");
        verify(request, times(1)).getParameter("type");
        verify(request, times(1)).getParameter("idZoo");
        verify(request, times(1)).getParameter("nameZoo");
        verify(animalDAO, times(0)).deleteByClassAndZoo(anyObject(), anyObject());
        verify(response, times(1)).sendRedirect(String.format("%s/animal_type", request.getContextPath()));
    }

    @Test(expected = ServletException.class)
    public void doPostException() throws Exception {
        doThrow(new DAOException()).when(animalDAO).deleteByClassAndZoo(anyObject(), anyObject());
        try {
            deleteAnimalClassFromAnimal.init();
            deleteAnimalClassFromAnimal.doPost(request, response);
        } catch (ServletException e) {
            verify(request, times(1)).getParameter("id");
            verify(request, times(1)).getParameter("type");
            verify(request, times(1)).getParameter("idZoo");
            verify(request, times(1)).getParameter("nameZoo");
            verify(animalDAO, times(1)).deleteByClassAndZoo(anyObject(), anyObject());
            verify(response, times(0)).sendRedirect(String.format("%s/animal_type", request.getContextPath()));
            throw e;
        }
    }
}