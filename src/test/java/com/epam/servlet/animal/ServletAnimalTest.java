package com.epam.servlet.animal;

import com.epam.database.exception.DAOException;
import com.epam.database.exception.DBConnectionException;
import com.epam.servlet.ServletMockTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import static com.epam.servlet.config.PathJSP.ANIMAL_JSP;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ServletAnimalTest extends ServletMockTest {
    private ServletAnimal servletAnimal;

    @Before
    public void setUp() throws DBConnectionException, DAOException {
        servletAnimal = new ServletAnimal() {
            @Override
            public ServletContext getServletContext() {
                return context;
            }
        };

        when(servletAnimal.getServletContext().getAttribute("zooDAO")).thenReturn(zooDAO);
        when(servletAnimal.getServletContext().getAttribute("animalClassDAO")).thenReturn(animalClassDAO);
        when(servletAnimal.getServletContext().getAttribute("animalDAO")).thenReturn(animalDAO);

        when(request.getParameter("idZoo")).thenReturn(String.valueOf(1));
        when(request.getParameter("nameZoo")).thenReturn("zoo-test");
        when(request.getParameter("idType")).thenReturn(String.valueOf(1));
        when(request.getParameter("type")).thenReturn("class-test");

        when(animalDAO.getAllByZooAndClass(anyObject(), anyObject())).thenReturn(listAnimals);
        when(zooDAO.getAll()).thenReturn(listZoos);
        when(animalClassDAO.getAll()).thenReturn(listClasses);

        doNothing().when(request).setAttribute("animals", listAnimals);
        doNothing().when(request).setAttribute("zoos", listZoos);
        doNothing().when(request).setAttribute("animalClasses", listClasses);

        when(request.getRequestDispatcher(ANIMAL_JSP.PATH)).thenReturn(dispatcher);
    }

    @Test
    public void initNoException() throws Exception {
        servletAnimal.init();

        verify(servletAnimal.getServletContext(), times(1)).getAttribute("zooDAO");
        verify(servletAnimal.getServletContext(), times(1)).getAttribute("animalClassDAO");
        verify(servletAnimal.getServletContext(), times(1)).getAttribute("animalDAO");
    }

    @Test
    public void doPostNoException() throws Exception {
        servletAnimal.init();
        servletAnimal.doPost(request, response);

        verify(request, times(1)).getParameter("idZoo");
        verify(request, times(1)).getParameter("nameZoo");
        verify(request, times(1)).getParameter("idType");
        verify(request, times(1)).getParameter("type");
        verify(response, times(1)).sendRedirect(String.format("%s/animal", request.getContextPath()));
    }

    @Test
    public void doGetNoException() throws Exception {
        servletAnimal.init();
        servletAnimal.doPost(request, response);
        servletAnimal.doGet(request, response);

        verify(animalDAO, times(1)).getAllByZooAndClass(anyObject(), anyObject());
        verify(zooDAO, times(1)).getAll();
        verify(animalClassDAO, times(1)).getAll();
        verify(request, times(1)).setAttribute("animals", listAnimals);
        verify(request, times(1)).setAttribute("zoos", listZoos);
        verify(request, times(1)).setAttribute("animalClasses", listClasses);
        verify(request, times(1)).getRequestDispatcher(ANIMAL_JSP.PATH);
        verify(dispatcher, times(1)).forward(request, response);
    }

    @Test(expected = ServletException.class)
    public void doGetAllZooException() throws Exception {
        when(animalClassDAO.getAll()).thenThrow(new DAOException());
        try {
            servletAnimal.init();
            servletAnimal.doPost(request, response);
            servletAnimal.doGet(request, response);
        } catch (ServletException e) {
            verify(animalDAO, times(1)).getAllByZooAndClass(anyObject(), anyObject());
            verify(zooDAO, times(1)).getAll();
            verify(animalClassDAO, times(1)).getAll();
            verify(request, times(0)).setAttribute("animals", listAnimals);
            verify(request, times(0)).setAttribute("zoos", listZoos);
            verify(request, times(0)).setAttribute("animalClasses", listClasses);
            verify(request, times(0)).getRequestDispatcher(ANIMAL_JSP.PATH);
            verify(dispatcher, times(0)).forward(request, response);
            throw e;
        }
    }
}