package com.epam.servlet.animal;

import com.epam.database.exception.DAOException;
import com.epam.servlet.ServletMockTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AddAnimalTest extends ServletMockTest {
    private AddAnimal addAnimal;

    @Before
    public void setUp() throws Exception {
        addAnimal = new AddAnimal() {
            @Override
            public ServletContext getServletContext() {
                return context;
            }
        };

        when(addAnimal.getServletContext().getAttribute("zooDAO")).thenReturn(zooDAO);
        when(addAnimal.getServletContext().getAttribute("animalClassDAO")).thenReturn(animalClassDAO);
        when(addAnimal.getServletContext().getAttribute("animalDAO")).thenReturn(animalDAO);

        when(request.getParameter("idZoo")).thenReturn(String.valueOf(1));
        when(request.getParameter("idType")).thenReturn(String.valueOf(1));
        when(request.getParameter("animalName")).thenReturn("animalName-test");
        when(request.getParameter("appearance")).thenReturn("appearance-test");

        when(zooDAO.getById(anyLong())).thenReturn(zoo);
        when(animalClassDAO.getById(anyLong())).thenReturn(animalClass);
        doNothing().when(animalDAO).insert(animal);
    }

    @Test
    public void initNoException() throws Exception {
        addAnimal.init();

        verify(addAnimal.getServletContext(), times(1)).getAttribute("zooDAO");
        verify(addAnimal.getServletContext(), times(1)).getAttribute("animalClassDAO");
        verify(addAnimal.getServletContext(), times(1)).getAttribute("animalDAO");
    }

    @Test
    public void doPostNoException() throws Exception {
        addAnimal.init();
        addAnimal.doPost(request, response);

        verify(request, times(1)).getParameter("idZoo");
        verify(request, times(1)).getParameter("idType");
        verify(request, times(1)).getParameter("animalName");
        verify(request, times(1)).getParameter("appearance");

        verify(zooDAO, times(1)).getById(anyLong());
        verify(animalClassDAO, times(1)).getById(anyObject());
        verify(animalDAO, times(1)).insert(anyObject());
        verify(response, times(0)).sendError(eq(HttpServletResponse.SC_BAD_REQUEST), anyString());
        verify(response, times(1)).sendRedirect(String.format("%s/", request.getContextPath()));
    }

    @Test
    public void doPostInvalidParam() throws Exception {
        when(request.getParameter("animalName")).thenReturn("");

        addAnimal.init();
        addAnimal.doPost(request, response);

        verify(request, times(1)).getParameter("idZoo");
        verify(request, times(1)).getParameter("idType");
        verify(request, times(1)).getParameter("animalName");
        verify(request, times(1)).getParameter("appearance");

        verify(zooDAO, times(0)).getById(anyLong());
        verify(animalClassDAO, times(0)).getById(anyObject());
        verify(animalDAO, times(0)).insert(anyObject());
        verify(response, times(1)).sendError(eq(HttpServletResponse.SC_BAD_REQUEST), anyString());
        verify(response, times(1)).sendRedirect(String.format("%s/", request.getContextPath()));
    }

    @Test(expected = ServletException.class)
    public void doPostExceptionGetByIdZoo() throws Exception {
        when(zooDAO.getById(anyLong())).thenThrow(new DAOException());
        try {
            addAnimal.init();
            addAnimal.doPost(request, response);
        } catch (ServletException e) {
            verify(request, times(1)).getParameter("idZoo");
            verify(request, times(1)).getParameter("idType");
            verify(request, times(1)).getParameter("animalName");
            verify(request, times(1)).getParameter("appearance");

            verify(zooDAO, times(1)).getById(anyLong());
            verify(animalClassDAO, times(0)).getById(anyObject());
            verify(animalDAO, times(0)).insert(anyObject());
            verify(response, times(0)).sendError(eq(HttpServletResponse.SC_BAD_REQUEST), anyString());
            verify(response, times(0)).sendRedirect(String.format("%s/", request.getContextPath()));
            throw e;
        }
    }

    @Test(expected = ServletException.class)
    public void doPostExceptionInsertAnimal() throws Exception {
        doThrow(new DAOException()).when(animalDAO).insert(anyObject());
        try {
            addAnimal.init();
            addAnimal.doPost(request, response);
        } catch (ServletException e) {
            verify(request, times(1)).getParameter("idZoo");
            verify(request, times(1)).getParameter("idType");
            verify(request, times(1)).getParameter("animalName");
            verify(request, times(1)).getParameter("appearance");

            verify(zooDAO, times(1)).getById(anyLong());
            verify(animalClassDAO, times(1)).getById(anyObject());
            verify(animalDAO, times(1)).insert(anyObject());
            verify(response, times(0)).sendError(eq(HttpServletResponse.SC_BAD_REQUEST), anyString());
            verify(response, times(0)).sendRedirect(String.format("%s/", request.getContextPath()));
            throw e;
        }
    }
}