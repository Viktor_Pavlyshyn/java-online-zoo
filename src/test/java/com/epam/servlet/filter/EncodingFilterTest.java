package com.epam.servlet.filter;

import com.epam.servlet.ServletMockTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.FilterChain;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class EncodingFilterTest extends ServletMockTest {
    private EncodingFilter encodingFilter;
    @Mock
    FilterChain filterChain;

    @Before
    public void setUp() throws Exception {
        encodingFilter = new EncodingFilter();

        doNothing().when(request).setCharacterEncoding("UTF-8");
        doNothing().when(response).setCharacterEncoding("UTF-8");

        doNothing().when(filterChain).doFilter(request, response);
    }

    @Test
    public void doFilter() throws Exception {
        encodingFilter.doFilter(request, response, filterChain);

        verify(request, times(1)).setCharacterEncoding("UTF-8");
        verify(response, times(1)).setCharacterEncoding("UTF-8");

        verify(filterChain, times(1)).doFilter(request, response);
    }
}