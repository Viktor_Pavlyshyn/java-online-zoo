package com.epam.servlet.animalClass;

import com.epam.database.exception.DAOException;
import com.epam.model.AnimalClass;
import com.epam.servlet.ServletMockTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import java.util.HashSet;
import java.util.LinkedList;

import static com.epam.servlet.config.PathJSP.ANIMALCLASS_JSP;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ServletAnimalClassTest extends ServletMockTest {
    private ServletAnimalClass servletAnimalClass;

    @Before
    public void setUp() throws Exception {
        servletAnimalClass = new ServletAnimalClass() {
            @Override
            public ServletContext getServletContext() {
                return context;
            }
        };
        //init()
        when(servletAnimalClass.getServletContext().getAttribute("animalDAO")).thenReturn(animalDAO);
        when(servletAnimalClass.getServletContext().getAttribute("animalClassDAO")).thenReturn(animalClassDAO);
        //doPost
        when(request.getParameter("id")).thenReturn(String.valueOf(1));
        when(request.getParameter("nameZoo")).thenReturn("test-nameZoo");
        //doGet
        setId = new HashSet<>();
        setId.add((long) 1);

        listClasses = new LinkedList<>();
        listClasses.add(new AnimalClass((long) 1));

        when(animalClassDAO.getAll()).thenReturn(listClasses);
        when(animalDAO.getAllClassIdByZoo(zoo)).thenReturn(setId);
        doNothing().when(request).setAttribute("zoo", zoo);
        doNothing().when(request).setAttribute("animalClasses", listClasses);
        when(request.getRequestDispatcher(ANIMALCLASS_JSP.PATH)).thenReturn(dispatcher);
    }

    @Test
    public void initNoException() throws Exception {
        servletAnimalClass.init();

        verify(servletAnimalClass.getServletContext(), times(1)).getAttribute("animalClassDAO");
        verify(servletAnimalClass.getServletContext(), times(1)).getAttribute("animalDAO");
    }

    @Test
    public void doPostNoException() throws Exception {
        servletAnimalClass.init();
        servletAnimalClass.doPost(request, response);

        verify(request, times(1)).getParameter("id");
        verify(request, times(1)).getParameter("nameZoo");
        verify(response, times(1)).sendRedirect(String.format("%s/animal_type", request.getContextPath()));
    }

    @Test
    public void doGetNoException() throws Exception {
        servletAnimalClass.init();
        servletAnimalClass.doGet(request, response);

        verify(animalClassDAO, times(1)).getAll();
        verify(animalDAO, times(1)).getAllClassIdByZoo(anyObject());
        verify(request, times(1)).setAttribute("zoo", zoo);
        verify(request, times(1)).setAttribute("animalClasses", listClasses);
        verify(request, times(1)).getRequestDispatcher(ANIMALCLASS_JSP.PATH);
        verify(dispatcher, times(1)).forward(request, response);
    }

    @Test(expected = ServletException.class)
    public void doGetAllAnimalClassException() throws Exception {
        when(animalClassDAO.getAll()).thenThrow(new DAOException());
        try {
            servletAnimalClass.init();
            servletAnimalClass.doGet(request, response);
        } catch (ServletException e) {
            verify(animalClassDAO, times(1)).getAll();
            verify(animalDAO, times(0)).getAllClassIdByZoo(anyObject());
            verify(request, times(0)).setAttribute("zoo", zoo);
            verify(request, times(0)).setAttribute("animalClasses", listClasses);
            verify(request, times(0)).getRequestDispatcher(ANIMALCLASS_JSP.PATH);
            verify(dispatcher, times(0)).forward(request, response);
            throw e;
        }
    }
}