package com.epam.servlet.animalClass;

import com.epam.database.exception.DAOException;
import com.epam.model.AnimalClass;
import com.epam.servlet.ServletMockTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import static com.epam.servlet.config.PathJSP.UPDATE_ANIMALCLASS_JSP;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UpdateAnimalClassTest extends ServletMockTest {
    private UpdateAnimalClass updateAnimalClass;

    @Before
    public void setUp() throws Exception {
        updateAnimalClass = new UpdateAnimalClass() {
            @Override
            public ServletContext getServletContext() {
                return context;
            }
        };
        //int()
        when(updateAnimalClass.getServletContext().getAttribute("animalClassDAO")).thenReturn(animalClassDAO);
        //doGet
        when(request.getParameter("idType")).thenReturn(String.valueOf(1));
        when(animalClassDAO.getById(1L)).thenReturn(animalClass);
        doNothing().when(request).setAttribute("animalClass", animalClass);
        when(request.getRequestDispatcher(UPDATE_ANIMALCLASS_JSP.PATH)).thenReturn(dispatcher);
        //doPost
        when(request.getParameter("idType")).thenReturn(String.valueOf(1));
        when(request.getParameter("type")).thenReturn("test-type");
        doNothing().when(animalClassDAO).update(animalClass);
        doNothing().when(response).sendError(eq(HttpServletResponse.SC_BAD_REQUEST), anyString());
    }

    @Test
    public void initNoException() throws Exception {
        updateAnimalClass.init();

        verify(updateAnimalClass.getServletContext(), times(1)).getAttribute("animalClassDAO");
    }

    @Test
    public void doGetNoException() throws Exception {
        updateAnimalClass.init();
        updateAnimalClass.doGet(request, response);

        verify(request, times(1)).getParameter("idType");
        verify(animalClassDAO, times(1)).getById(1L);
        verify(request, times(1)).setAttribute("animalClass", animalClass);
        verify(response, times(0)).sendRedirect(String.format("%s/", request.getContextPath()));
        verify(request, times(1)).getRequestDispatcher(UPDATE_ANIMALCLASS_JSP.PATH);
        verify(dispatcher, times(1)).forward(request, response);
    }

    @Test
    public void doGetInvalidId() throws Exception {
        when(request.getParameter("idType")).thenReturn("test-id");
        updateAnimalClass.init();
        updateAnimalClass.doGet(request, response);

        verify(request, times(1)).getParameter("idType");
        verify(animalClassDAO, times(0)).getById(1L);
        verify(request, times(0)).setAttribute("animalClass", animalClass);
        verify(response, times(1)).sendRedirect(String.format("%s/", request.getContextPath()));
        verify(request, times(0)).getRequestDispatcher(UPDATE_ANIMALCLASS_JSP.PATH);
        verify(dispatcher, times(0)).forward(request, response);
    }

    @Test(expected = ServletException.class)
    public void doGetByIdException() throws Exception {
        when(animalClassDAO.getById(1L)).thenThrow(new DAOException());
        try {
            updateAnimalClass.init();
            updateAnimalClass.doGet(request, response);
        } catch (ServletException e) {
            verify(request, times(1)).getParameter("idType");
            verify(animalClassDAO, times(1)).getById(1L);
            verify(request, times(0)).setAttribute(eq("animalClass"), any(AnimalClass.class));
            verify(response, times(0)).sendRedirect(String.format("%s/", request.getContextPath()));
            verify(request, times(0)).getRequestDispatcher(eq(UPDATE_ANIMALCLASS_JSP.PATH));
            verify(dispatcher, times(0)).forward(request, response);
            throw e;
        }
    }

    @Test
    public void doPostInvalidParam() throws Exception {
        when(request.getParameter("type")).thenReturn("");

        updateAnimalClass.init();
        updateAnimalClass.doPost(request, response);

        verify(request, times(1)).getParameter("idType");
        verify(request, times(1)).getParameter("type");
        verify(animalClassDAO, times(0)).update(anyObject());
        verify(response, times(1)).sendError(eq(HttpServletResponse.SC_BAD_REQUEST), anyString());
        verify(response, times(0)).sendRedirect(String.format("%s/", request.getContextPath()));
    }

    @Test
    public void doPostNoException() throws Exception {
        updateAnimalClass.init();
        updateAnimalClass.doPost(request, response);

        verify(request, times(1)).getParameter("idType");
        verify(request, times(1)).getParameter("type");
        verify(animalClassDAO, times(1)).update(anyObject());
        verify(response, times(0)).sendError(eq(HttpServletResponse.SC_BAD_REQUEST), anyString());
        verify(response, times(1)).sendRedirect(String.format("%s/", request.getContextPath()));
    }

    @Test(expected = ServletException.class)
    public void doPostUpdateException() throws Exception {
        doThrow(new DAOException()).when(animalClassDAO).update(anyObject());
        try {
            updateAnimalClass.init();
            updateAnimalClass.doPost(request, response);
        } catch (ServletException e) {
            verify(request, times(1)).getParameter("idType");
            verify(request, times(1)).getParameter("type");
            verify(animalClassDAO, times(1)).update(anyObject());
            verify(response, times(0)).sendError(eq(HttpServletResponse.SC_BAD_REQUEST), anyString());
            verify(response, times(0)).sendRedirect(String.format("%s/", request.getContextPath()));
            throw e;
        }
    }
}