package com.epam.servlet.animalClass;

import com.epam.database.exception.DAOException;
import com.epam.servlet.ServletMockTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DeleteAnimalClassTest extends ServletMockTest {
    private DeleteAnimalClass deleteAnimalClass;

    @Before
    public void setUp() throws Exception {
        deleteAnimalClass = new DeleteAnimalClass() {
            @Override
            public ServletContext getServletContext() {
                return context;
            }
        };
        //init
        when(deleteAnimalClass.getServletContext().getAttribute("animalClassDAO")).thenReturn(animalClassDAO);
        when(deleteAnimalClass.getServletContext().getAttribute("animalDAO")).thenReturn(animalDAO);
        //doPost
        when(request.getParameter("typeId")).thenReturn(String.valueOf(1));
        doNothing().when(animalDAO).deleteAllByClass(animalClass);
        doNothing().when(animalClassDAO).delete(animalClass);
    }

    @Test
    public void initNoException() throws Exception {
        deleteAnimalClass.init();

        verify(deleteAnimalClass.getServletContext(), times(1)).getAttribute("animalClassDAO");
        verify(deleteAnimalClass.getServletContext(), times(1)).getAttribute("animalDAO");
    }

    @Test
    public void doPostNoException() throws Exception {
        deleteAnimalClass.init();
        deleteAnimalClass.doPost(request, response);

        when(request.getParameter("typeId")).thenReturn(String.valueOf(1));
        verify(animalDAO, times(1)).deleteAllByClass(anyObject());
        verify(animalClassDAO, times(1)).delete(anyObject());
        verify(response, times(1)).sendRedirect(String.format("%s/", request.getContextPath()));
    }

    @Test
    public void doPosInvalidId() throws Exception {
        when(request.getParameter("typeId")).thenReturn("test-id");
        deleteAnimalClass.init();
        deleteAnimalClass.doPost(request, response);

        when(request.getParameter("typeId")).thenReturn(String.valueOf(1));
        verify(animalDAO, times(0)).deleteAllByClass(anyObject());
        verify(animalClassDAO, times(0)).delete(anyObject());
        verify(response, times(1)).sendRedirect(String.format("%s/", request.getContextPath()));
    }

    @Test(expected = ServletException.class)
    public void doPosDeleteAnimalClassException() throws Exception {
        doThrow(new DAOException()).when(animalClassDAO).delete(anyObject());
        try {
            deleteAnimalClass.init();
            deleteAnimalClass.doPost(request, response);
        } catch (ServletException e) {
            when(request.getParameter("typeId")).thenReturn(String.valueOf(1));
            verify(animalDAO, times(1)).deleteAllByClass(anyObject());
            verify(animalClassDAO, times(1)).delete(anyObject());
            verify(response, times(0)).sendRedirect(String.format("%s/", request.getContextPath()));
            throw e;
        }
    }

}