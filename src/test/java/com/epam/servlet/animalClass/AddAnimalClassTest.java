package com.epam.servlet.animalClass;

import com.epam.database.exception.DAOException;
import com.epam.servlet.ServletMockTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AddAnimalClassTest extends ServletMockTest {
    private AddAnimalClass addAnimalClass;

    @Before
    public void setUp() throws Exception {
        addAnimalClass = new AddAnimalClass() {
            @Override
            public ServletContext getServletContext() {
                return context;
            }
        };

        when(addAnimalClass.getServletContext().getAttribute("animalClassDAO")).thenReturn(animalClassDAO);
        when(request.getParameter("type")).thenReturn("test-type");

        doNothing().when(animalClassDAO).insert(animalClass);
        doNothing().when(response).sendError(eq(HttpServletResponse.SC_BAD_REQUEST), anyString());
    }

    @Test
    public void initNoException() throws Exception {
        addAnimalClass.init();

        verify(addAnimalClass.getServletContext(), times(1)).getAttribute("animalClassDAO");
    }

    @Test
    public void doPostNoException() throws Exception {
        addAnimalClass.init();
        addAnimalClass.doPost(request, response);

        verify(request, times(1)).getParameter("type");
        verify(animalClassDAO, times(1)).insert(anyObject());
        verify(response, times(0)).sendError(eq(HttpServletResponse.SC_BAD_REQUEST), anyString());
        verify(response, times(1)).sendRedirect(String.format("%s/", request.getContextPath()));
    }

    @Test
    public void doPostInvalidParam() throws Exception {
        when(request.getParameter("type")).thenReturn("");

        addAnimalClass.init();
        addAnimalClass.doPost(request, response);

        verify(request, times(1)).getParameter("type");
        verify(animalClassDAO, times(0)).insert(anyObject());
        verify(response, times(1)).sendError(eq(HttpServletResponse.SC_BAD_REQUEST), anyString());
        verify(response, times(1)).sendRedirect(String.format("%s/", request.getContextPath()));
    }

    @Test(expected = ServletException.class)
    public void doPostException() throws Exception {
        doThrow(new DAOException()).when(animalClassDAO).insert(anyObject());
        try {
            addAnimalClass.init();
            addAnimalClass.doPost(request, response);
        } catch (ServletException e) {
            verify(request, times(1)).getParameter("type");
            verify(animalClassDAO, times(1)).insert(anyObject());
            verify(response, times(0)).sendError(eq(HttpServletResponse.SC_BAD_REQUEST), anyString());
            verify(response, times(0)).sendRedirect(String.format("%s/", request.getContextPath()));
            throw e;
        }
    }
}