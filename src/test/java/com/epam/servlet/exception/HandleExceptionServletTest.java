package com.epam.servlet.exception;

import com.epam.servlet.ServletMockTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.epam.servlet.config.PathJSP.HANDLE_EXCEPTION_JSP;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class HandleExceptionServletTest extends ServletMockTest {
    private HandleExceptionServlet handleExceptionServlet;
    @Mock
    private
    Throwable throwable;

    @Before
    public void setUp() throws Exception {
        handleExceptionServlet = new HandleExceptionServlet();

        when(request.getAttribute("javax.servlet.error.exception")).thenReturn(throwable);
        when(request.getAttribute("javax.servlet.error.status_code")).thenReturn(1);
        when(request.getAttribute("javax.servlet.error.servlet_name")).thenReturn("servlet_name");

        doNothing().when(request).setAttribute(eq("statusCode"), anyString());
        doNothing().when(request).setAttribute(eq("servletName"), anyString());
        doNothing().when(request).setAttribute(eq("exceptionType"), anyString());
        doNothing().when(request).setAttribute(eq("exceptionMessage"), anyString());

        when(request.getRequestDispatcher(HANDLE_EXCEPTION_JSP.PATH)).thenReturn(dispatcher);
    }

    @Test
    public void doGet() throws Exception {
        handleExceptionServlet.doGet(request, response);

        verify(request, times(1)).getAttribute("javax.servlet.error.exception");
        verify(request, times(1)).getAttribute("javax.servlet.error.status_code");
        verify(request, times(1)).getAttribute("javax.servlet.error.servlet_name");

        verify(request, times(1)).setAttribute(eq("statusCode"), anyString());
        verify(request, times(1)).setAttribute(eq("servletName"), anyString());
        verify(request, times(1)).setAttribute(eq("exceptionType"), anyString());
        verify(request, times(1)).setAttribute(eq("exceptionMessage"), anyString());

        verify(request, times(1)).getRequestDispatcher(HANDLE_EXCEPTION_JSP.PATH);
        verify(dispatcher, times(1)).forward(request, response);
    }


}