Practical_tasks

Personal project task description:

Індивідуальне завдання: 11. Зоопарк.

1.	Розробити доменну модель відповідно до індивідуального завдання.
a.	Застосувати різні типи колекцій, для прикладу LinkedList, ArrayList, HashMap, інші;
b.	Використати Enums.
Діаграма-приклад:

2.	Класи і методи повинні мати назви, які відображають їхню функціональність,
 і повинні бути структуровані відповідно по пакетах.
3.	Використати Log4J для логування операцій над сутностями.
4.	Exceptions повинні буди збереженні в файл за допомогою Log4J.
a.	Застосувати try-catch для checked Exceptions;
b.	Розробити кастомізовані Exceptions;
c.	Використати single and multi catch конструкції для опрацювання Exceptions.
5.	Заповнити базу даних (In-Memory DB H2) та забезпечити збереження доменної моделі у базі даних.
6.	Розробити CRUD API за допомогою Servlet 3.0 API, операції:
Create - API для створення нової сутності;
Read - API для отримання даних по одній/усім сутностям;
Update - API для модифікації конкретної сутності;
Delete - API для видалення конкретної сутності.
7.	Застосувати трьох-рівневу структуру Model-View-Controller в системі.
8.	Розробити користувацький інтерфейс з можливістю відображення списку збережених сутностей,
 відображення та редагування детальної інформації по конкретній сутності,
  видалення та створення нових сутностей.
9.	Покрити функціонал юніт тестами з використанням Junit та Mockito, рівень покриття повинен бути > 80%.


How to deploy and run application.

To run the Zoo app, you need:

-  run this command: "mvn jetty:run" (you will use default database - H2);

-  if not speciality H2 database will be use. In order to change database you have to add

property "-DdbName=MySQL". At the moment H2 and MySQL are supported.;

- after Started Jetty Server, application will be available by this path: "http://localhost:8080/zoo/";

To check the test coverage, you need:

-  run this command: "mvn clean package";

-  after success build, open "index.html", following this path: "\zoo\target\site\jacoco\index.html".
